<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group(['middleware' => 'auth:api'], function () {

    //---------------------------- User Routes
    Route::group(["prefix" => "user"], function () {

        Route::post('/register', 'Api\UserController@store');
        Route::post('/login', 'Api\UserController@login');
        
        // Apply Middle ware on user authentication
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('details', 'Api\UserController@details');
            Route::post('{Id}/assign-orders', 'Api\OrderController@storeOrder');
        });

    });

    //---------------------------- Rider Routes
    Route::group(["prefix" => "rider"], function () {

        Route::post('/register', 'Api\RiderController@store');
        Route::post('/login', 'Api\RiderController@login');
        
        // Apply Middle ware on user authentication
        Route::group(['middleware' => 'auth:rider'], function(){
            Route::get('details', 'Api\RiderController@details');
            Route::get('{Id}/assign-orders', 'Api\RiderController@getAssignedOrders');
            Route::post('order/delivery-status', 'Api\RiderController@deliveryStatus');
            Route::post('order-collect/status', 'Api\RiderController@collectionStatus');
        });
    });

    // Route::group([ 'middleware' => 'api', 'prefix' => 'password' ], function () {    
        Route::post('create', 'Api\PasswordResetController@create');
        Route::get('find/{token}', 'Api\PasswordResetController@find');
        Route::post('reset', 'Api\PasswordResetController@reset');
    // });

    //---------------------------- Categories Routes
    Route::group(["prefix" => "categories"], function () {

        Route::get('/', 'Api\CategoryController@getAll');
        Route::get('/featured', 'Api\CategoryController@getAllFeatured');
        Route::get('/main-screen', 'Api\CategoryController@getMainScreenCategory');
    });

    //---------------------------- Promotions Routes
    Route::group(["prefix" => "promotions"], function () {

        Route::get('/', 'Api\PromotionController@getAll');
        Route::get('/main-screen', 'Api\PromotionController@getMainScreenPromotion');
    });

    //---------------------------- Sliders Routes
    Route::group(["prefix" => "sliders"], function () {

        Route::get('/', 'Api\SiteController@getAllSliders');
    });

    //---------------------------- Discounts Routes
    Route::group(["prefix" => "discounts"], function () {

        Route::get('/', 'Api\SiteController@getAllDiscounts');
    });

    //---------------------------- Products Routes
    Route::group(["prefix" => "products"], function () {

        Route::get('/', 'Api\ProductController@getAll');
        Route::get('/featured', 'Api\ProductController@getAllFeatured');
        Route::get('/customize-box', 'Api\ProductController@getCustomizeBoxProduct');
        Route::get('/category/{categoryId}', 'Api\ProductController@getSpecifiedProduct');
        Route::get('/detail/{productId}', 'Api\ProductController@getProductDetail');
	Route::get('/hashlob/web-data/products/get/all/auth','Api\ProductController@getAll');        
    });

    //---------------------------- Orders Routes
    Route::group(["prefix" => "orders"], function () {

        Route::post('/store', 'Api\OrderController@storeOrder');
        Route::get('/{userId}','Api\OrderController@getAllOrders');
        Route::get('/detail/{orderNo}','Api\OrderController@getOrderDetail');
        Route::post('/status/{orderNo}','OrderController@status');
        Route::post('/rider-details/store','Api\OrderController@getRiderOrderDetail');
    });


    // Route::post('/payment-response',function(Request $request){
    //     if(!empty($request->all()))
    //     {
    //         return response()->json([
    //             "code"      =>  200,
    //             "message"   =>  "Response Recieved",
    //         ], 200);
    //     }
    //     return response()->json([
    //         "code"      =>  409,
    //         "message"   =>  "Response not recieved",
    //     ], 409);
    // });
    Route::post('/payment-response','PaymentController@confirm');
    Route::get('/transaction/all','PaymentController@transactions');
// });

Route::any('{url?}/{sub_url?}', function () {
    return result(false, 404, "Route not Found.");
});
