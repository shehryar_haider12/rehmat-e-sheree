<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/login');
});

Route::group(['prefix' => 'transactions'], function () {
    Route::get('/','PaymentController@transactionAll')->name('transaction');
    Route::get('/datatable','PaymentController@transactionDatatable')->name('transaction.datatable');
});

Route::group(['middleware' => ['auth']], function () {
    
// Route::Categories
Route::get('/category','CategoryController@index')->name('categories');
Route::get('/category/datatable','CategoryController@datatable')->name('category.datatable');
Route::get('/category/create','CategoryController@create')->name('category.create');
Route::post('/category/store','CategoryController@store')->name('category.store');
Route::get('/category/edit/{category}','CategoryController@edit')->name('category.edit');
Route::put('/category/update/{category}','CategoryController@update')->name('category.update');
Route::patch('/category/status','CategoryController@status')->name('category.status');
Route::delete('/category/delete','CategoryController@destroy')->name('category.delete');

// Route::Products
Route::get('/product','ProductController@index')->name('products');
Route::get('/product/datatable','ProductController@datatable')->name('product.datatable');
Route::get('/product/create','ProductController@create')->name('product.create');
Route::post('/product/store','ProductController@store')->name('product.store');
Route::get('/product/edit/{product}','ProductController@edit')->name('product.edit');
Route::put('/product/update/{product}','ProductController@update')->name('product.update');
Route::patch('/product/status','ProductController@status')->name('product.status');
Route::delete('/product/delete','ProductController@destroy')->name('product.delete');
Route::get('/product/images/{product_image}','ProductController@destroyProductImage')->name('product_image.delete');

// Route::Units
Route::get('/units','UnitController@index')->name('units');
Route::get('/units/datatable','UnitController@datatable')->name('unit.datatable');
Route::get('/units/create','UnitController@create')->name('unit.create');
Route::post('/units/store','UnitController@store')->name('unit.store');
Route::get('/units/edit/{unit}','UnitController@edit')->name('unit.edit');
Route::put('/units/update/{unit}','UnitController@update')->name('unit.update');
Route::patch('/units/status','UnitController@status')->name('unit.status');
Route::delete('/units/delete','UnitController@destroy')->name('unit.delete');

// Route::Promotions
Route::get('/promotion','PromotionController@index')->name('promotions');
Route::get('/promotion/datatable','PromotionController@datatable')->name('promotion.datatable');
Route::get('/promotion/create','PromotionController@create')->name('promotion.create');
Route::post('/promotion/store','PromotionController@store')->name('promotion.store');
Route::get('/promotion/edit/{promotion}','PromotionController@edit')->name('promotion.edit');
Route::get('/promotion/image/delete/{promotion}','PromotionController@deleteImage')->name('promotion.delete-image');
Route::put('/promotion/update/{promotion}','PromotionController@update')->name('promotion.update');
Route::patch('/promotion/status','PromotionController@status')->name('promotion.status');
Route::delete('/promotion/delete','PromotionController@destroy')->name('promotion.delete');

// Route::Sliders
Route::get('/slider','SliderController@index')->name('sliders');
Route::get('/slider/datatable','SliderController@datatable')->name('slider.datatable');
Route::get('/slider/create','SliderController@create')->name('slider.create');
Route::post('/slider/store','SliderController@store')->name('slider.store');
Route::get('/slider/image/delete/{slider}','SliderController@deleteImage')->name('slider.delete-image');
Route::get('/slider/edit/{slider}','SliderController@edit')->name('slider.edit');
Route::put('/slider/update/{slider}','SliderController@update')->name('slider.update');
Route::patch('/slider/status','SliderController@status')->name('slider.status');
Route::delete('/slider/delete','SliderController@destroy')->name('slider.delete');

// Route::Customers
Route::get('/customers','CustomerController@index')->name('customers');
Route::get('/customers/datatable','CustomerController@datatable')->name('customer.datatable');
Route::patch('/customers/status','CustomerController@status')->name('customer.status');

// Route::Riders
Route::get('/riders','RiderController@index')->name('riders');
Route::get('/riders/datatable','RiderController@datatable')->name('rider.datatable');
Route::get('/riders/create','RiderController@create')->name('riders.create');
Route::post('/riders/store','RiderController@store')->name('riders.store');
Route::get('/riders/edit/{riders}','RiderController@edit')->name('riders.edit');
Route::put('/riders/update/{riders}','RiderController@update')->name('riders.update');
Route::delete('/riders/delete','RiderController@destroy')->name('riders.delete');
Route::patch('/rider/status','RiderController@status')->name('rider.status');


// Route::Discounts
Route::get('/discounts','DiscountController@index')->name('discounts');
Route::get('/discounts/datatable','DiscountController@datatable')->name('discount.datatable');
Route::get('/discounts/create','DiscountController@create')->name('discount.create');
Route::post('/discounts/store','DiscountController@store')->name('discount.store');
Route::get('/discounts/edit/{discount}','DiscountController@edit')->name('discount.edit');
Route::get('/discounts/image/delete/{discount}','DiscountController@deleteImage')->name('discount.delete-image');
Route::put('/discounts/update/{discount}','DiscountController@update')->name('discount.update');
Route::patch('/discounts/status','DiscountController@status')->name('discount.status');
Route::delete('/discounts/delete','DiscountController@destroy')->name('discount.delete');

// Route::Notifications
Route::get('/notification','NotificationController@index')->name('notification');
Route::get('/notification/datatable','NotificationController@datatable')->name('notification.datatable');
Route::get('/notification/create','NotificationController@create')->name('notification.create');
Route::post('/notification/store','NotificationController@store')->name('notification.store');
Route::get('/notification/view/{notification}','NotificationController@view')->name('notification.view');
Route::patch('/notification/read','NotificationController@read')->name('notification.read');
Route::get('/notification/resend/{id}','NotificationController@resend')->name('notification.resend');

// Route::Dashboard
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard/settings', 'HomeController@settings')->name('users.settings');
Route::patch('/dashboard/settings', 'HomeController@patch')->name('users.settings');

// Route::Orders
Route::get('/orders','OrderController@index')->name('orders');
Route::get('/orders/datatable','OrderController@datatable')->name('order.datatable');
Route::get('/order/invoice/{order_no}','OrderController@orderInvoice')->name('order.invoice');
Route::post('/orders/status','OrderController@status')->name('order.status');
Route::get('/orders/{order_no}/details','OrderController@orderDetail')->name('order.detail');
Route::get('/orders/{order_no}/datatable','OrderController@orderDetailDatatable')->name('order.detail.datatable');

// Rider Assign Order
Route::get('/rider/assign-orders','OrderController@riderAssignOrder')->name('rider.assign-orders');
Route::get('/rider/assign-orders/datatable','OrderController@riderAssignOrderdatatable')->name('rider.assign-orders.datatable');

// Route::Site Settings
Route::get('/site/settings', 'SiteSettingController@index')->name('site.settings');
Route::post('/site/settings/store', 'SiteSettingController@store')->name('site.settings.store');

// Route::Site Reports
Route::get('/reports', 'ReportsController@index')->name('reports');
Route::post('/reports/report', 'ReportsController@report')->name('search.reports');
Route::get('/reports/report/pdf', 'ReportsController@reportPdf')->name('generate.report.pdf');

});
// Password Reset Routes...
Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'ResetPasswordController@reset');

Route::get('exzed/cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
   return 'cache clear';
});

Auth::routes();