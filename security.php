<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', 'cfb2ab1bbef54f5286da4e57979738f5bed2678b75f74949a817e6227735ad254748e3e1e7b54f75aece871514957e2eace539a8c46044ba9664fafca66fbae5a48dad59bedf4113999852ef8e621d5d86722bfbc083498bb2397c14f4acb79d0ab5d8f01d914fe6856470725c31097ecc3a249e3a5b416dba2a88edb231ea9f');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
