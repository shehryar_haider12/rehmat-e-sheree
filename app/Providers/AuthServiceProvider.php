<?php

namespace App\Providers;

use Laravel\Passport\Passport; 
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->registerPolicies();
        Passport::routes(); 
        // Passport::tokensExpireIn(now()->addMinutes(1));
        // Passport::personalAccessTokensExpireIn(Carbon::now()->addMinutes(1));

        // Passport::refreshTokensExpireIn(now()->addMinutes(1));
        //
    }
}
