<?php
use App\Models\Setting;

 function result($data, $status, $message)
 {
    return response()->json([
        "data"      =>  $data,
        "code"      =>  $status,
        "message"   =>  $message,
        "image_url" =>  url('')."/uploads/",
    ], $status);
 }

 function getSiteSetting($key)
 {
    $setting =  Setting::where('key',$key)->first();
    if(!empty($setting))
    {
       return $setting->value;
    }
    return '';
 }