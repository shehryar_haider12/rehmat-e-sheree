<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\RiderDetail;
use App\Models\RiderAssignOrder;
use App\Models\Notification;
use App\Models\WebUser;
use App\User;
use Validator;
use Storage;
use Str;

class OrderController extends Controller
{           
    public $successStatus = 200;
    public $errorStatus = 401;

    public function storeOrder(Request $request)
    { 
//	    return $request->all();
        $data =  preg_replace('/\\\\/', '', $request->COItems);
        // return $request->all();
        // Storage::disk('uploads')->put('hello3',$info);
        $validator = Validator::make($request->all(),[ 

            'userId'                =>  'required',
            // 'ClientInformationId'   =>  'required',
            'phone'                 =>  'required|string',
            'address'               =>  'required|string|max:255',
            // 'OrderDeliveryTypeId'   =>  'required',
            // 'DeliveryCityArea'      =>  'required',
            // 'DeliveryCity'          =>  'required',
            // 'DeliveryCountry'       =>  'required',
            // 'OrderDiscount'         =>  'required',
            // 'OrderAmount'           =>  'required',
            // 'PaymentModeId'         =>  'required',
            // 'OrderRemarks'          =>  'required',
            // 'ReferenceNumber'       =>  'required',
            // 'CompanyBranchId'       =>  'required',
            // 'DeliveryBranchId'      =>  'required',

        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        } 
//	return "Recieved Ok";     
	$today          =   date("Ymd");
        $rand           =   strtoupper(substr(uniqid(sha1(time())),0,4));
        $order_no       =   $today . $rand;

        $info = json_decode($data);
        //return $info;
        //Product::where('id',)
        foreach($info as $key => $items)
        { 
            $product = Product::where('id',$items->ProductItemId)->first();	
            //return $product;
            $order_data = [
                    'order_no'              =>  $order_no,
                    'product_id'            =>  $items->ProductItemId,
                    'quantity'              =>  $items->Quantity,
                    'amount'                =>  $product->price * $items->Quantity,
                ];
                $create_order = Order::create($order_data);
                // $create_order = true;
        } 
      //return $create_order;	
        if($create_order)
        {   
            $amount = Order::where('order_no',$order_no)->pluck('amount')->toArray();
            $total = array_sum($amount);  
            if($total > getSiteSetting('free_delievery_charges_after'))
            {
                $total      =   $total + getSiteSetting('delievery_charges');
            }
            $delievery  =   getSiteSetting('delievery_charges');
            $client     =   new \GuzzleHttp\Client();
            
            // $URI = 'http://tsminingapi.azurewebsites.net/TSBE/CustomerSignUp/PushOrderData/RS/superuser/techSupport20177';
            
            // $params['form_params'] = [

            //     "ClientInformationId"    =>     $request->ClientInformationId,
            //     "OrderDate"              =>     $request->OrderDate,
            //     "DeliveryDate"           =>     $request->DeliveryDate,
            //     "OrderDeliveryTypeId"    =>     $request->OrderDeliveryTypeId,
            //     "DeliveryAddress"        =>     $request->DeliveryAddress,
            //     "DeliveryCityArea"       =>     "abc",
            //     "DeliveryCity"           =>     $request->DeliveryCity,
            //     "DeliveryCountry"        =>     $request->DeliveryCountry,
            //     "OrderDiscount"          =>     $request->OrderDiscount,
            //     "OrderAmount"            =>     $request->OrderAmount,
            //     "PaymentModeId"          =>     $request->PaymentModeId,
            //     "OrderRemarks"           =>     $request->OrderRemarks,
            //     "ReferenceNumber"        =>     $request->ReferenceNumber,
            //     "CompanyBranchId"        =>     $request->CompanyBranchId,
            //     "DeliveryBranchId"       =>     $request->DeliveryBranchId,
            //     "PaymentModeAmount"      =>     $request->PaymentModeAmount,
            //     "MerchantId"             =>     $request->MerchantId,
            // ];
 
            // $params['form_params']['COItems'] = $info;
            // $full = json_encode($params['form_params']);
            // // return $params;
            // // return $full;
            // $response = $client->post($URI, [
            //     'body'          => $full,
            //     'headers'       => [
            //         'Content-Type'  => 'application/json',
            //     ]
            // ]);
            // $technosys_data = $response->getBody();

            // $exp = explode(',',$technosys_data);
            // $exp2 = explode(':',$exp[1]);
            // $exp3 = explode('and',$exp2[1]);
            // $technosys_order_no = str_replace('"','',$exp2[1]);
            
            $details = [
                'order_no'                  =>  $order_no,
                'client_order_id'           =>  '1',
                'technosys_order_no'        =>  '1',
                'user_id'                   =>  $request->userId,
                'order_deliver_type_id'     =>  $request->OrderDeliveryTypeId,
                'payment_mode_id'           =>  $request->PaymentModeId,
                'delievery_charges'         =>  $delievery,
                'total_amount'              =>  $total,
                'phone'                     =>  $request->phone,     
                'address'                   =>  $request->address,
                'address_lat'               =>  $request->address_lat,
                'address_long'              =>  $request->address_long,
                'date'                      =>  date('Y-m-d'),
            ];
            OrderDetail::create($details);
            $admin      =  User::get();
            $customer   =  WebUser::find($request->userId);
            $from       =  'Rehmat-e-Sheeren';
            $message    =  'New Order from '.$customer->name.' order no '.$details['technosys_order_no']. ' at '.$request->OrderDate;

            foreach ($admin as $key => $sender) {
                $data = [
                    'from'      =>  $request->userId,
                    'to'        =>  $sender->id,
                    'message'   =>  $message,
                ];
                Notification::create($data);
            }
            $notification = new Notification;
            $notification->toMultiDevice($admin, $customer->name,$message,null,null);
            // return $from;
            // $notification->toSingleDevice(Auth::user()->device_token, $from,$request->message,null,null);
                
            return result($details,$this->successStatus,"Order Successfully Stored.");
        }   
        return result('error',$this->errorStatus,"Failed to Stored.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllOrders(Request $request)
    {
        $orders = OrderDetail::with('orders.product')->whereNull('deleted_at')->where('user_id',$request->userId)->orderBy('created_at','desc')->get();
        // $orders['image_url'] = url('')."/uploads/";
        return result($orders, $this->successStatus, "Orders.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRiderOrders(Request $request)
    {
        $orders = OrderDetail::with('orders.product')->whereNull('deleted_at')->where('user_id',$request->userId)->orderBy('created_at','desc')->get();
        // $orders['image_url'] = url('')."/uploads/";
        return result($orders, $this->successStatus, "Orders.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrderDetail(Request $request)
    { 
        $order_detail = OrderDetail::with('orders.product')->whereNull('deleted_at')->where('order_no',$request->orderNo)->first();
        return result($order_detail, $this->successStatus, "Order Detail.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRiderOrderDetail(Request $request)
    {
        $validator = Validator::make($request->all(),[ 

            'rider_id'  =>  'required',
            'order_no'  =>  'required|string|max:255',
            'name'      =>  'required|string|max:255',
            'contact'   =>  'required|string|max:15',

        ]);
        if ($validator->fails())
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        $rider = RiderDetail::where('rider_id',$request->rider_id)->first();
        if(!$rider)
        {   
            $rider = RiderDetail::create($request->except('status','error'));
        }
        $data = [
            'rider_id'              => $request->rider_id,
            'technosys_order_no'    => $request->order_no,
            'delivery_status'       => $request->status,
            'error'                 => $request->error ?? '',
        ];
        $admin   = User::first();
        $message = "New Order Assigned from Admin to you";
        //  Order Notifiation to Rider
        $notify_detail = [
            'from'      =>  $admin->id,
            'to'        =>  $rider->id,
            'message'   =>  $message,
        ];
        Notification::create($notify_detail);
        $notification = new Notification;
        $from       =  'Rehmat-e-Sheeren';
        $notification->toMultiDevice($rider->device_token, $from,$message,null,null);
        
        $check = RiderAssignOrder::where('technosys_order_no',$request->order_no)->whereIn('delivery_status',['in-process','on-way','delivered'])->count();
        if($check > 0)
        {
            $response['error'] = 'failed';
            $response['message'] = 'Rider Already Assigned to this order';
            return response()->json($response, 409);
        }
        else{
            RiderAssignOrder::create($data);
            $response['success'] = 'success';
            $response['message'] = 'Rider Details Recieved.';
            return response()->json($response, $this->successStatus);
        }
    }
}
