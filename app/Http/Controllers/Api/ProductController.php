<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryProduct;
use App\Models\Product;

class ProductController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $products = Product::get()->take(20);
      //  $products['image_url'] = url('')."/uploads/";

        return result($products, $this->successStatus, "Products.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomizeBoxProduct()
    {
        $customize_box_products = Product::with('categoryProduct')->where([['status',1],['customize_box',1]])->whereNull('deleted_at')->get();
      //  $customize_box_products['image_url'] = url('')."/uploads/";

        return result($customize_box_products, $this->successStatus, "Customize Box Products.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllFeatured()
    {
        $products = Product::with('categoryProduct')->where([['status',1],['is_featured',1]])->whereNull('deleted_at')->orderBy('created_at','desc')->get();
      //  $products['image_url'] = url('')."/uploads/";

        return result($products, $this->successStatus, "Featured Products.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSpecifiedProduct(Request $request)
    {
        $category_id        =   $request->categoryId;
        $product_id         =   CategoryProduct::with('products')->where('category_id',$category_id)->pluck('product_id');
        $specified_product  =   Product::whereIn('id',$product_id)->whereNull('deleted_at')->where('status',1)->orderBy('created_at','desc')->get();
        return result($specified_product, $this->successStatus, "Specified Products.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductDetail(Request $request)
    {
        $product_id         =   $request->productId;
        $product_detail     =   Product::with('productImages','Unit')->where('id',$product_id)->first();
    //    $product_detail['image_url'] = url('')."/uploads/";

        return result($product_detail, $this->successStatus, "Product Detail.");
    }
}
