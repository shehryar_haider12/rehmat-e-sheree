<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RiderDetail;
use App\Models\RiderAssignOrder;
use App\Models\Notification;
use Validator;
use App\User;
use Auth;
use Crypt;
use Hash;

class RiderController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    /**
     * Store Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(),[ 
            'contact'                   =>  'required|max:15',
            'password'                  =>  'required|confirmed',
            'password_confirmation'     =>  'required',
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        $rider = RiderDetail::where('contact',$request->contact)->first();
        if(!empty($rider)){
            $input              =   $request->all(); 
            $password           =   Hash::make($input['password']); 
            $rider->update(['password'=>$password,'device_token'=>$request->device_token]); 
            $success['token']   =   $rider->createToken('MyApp')->accessToken; 
            $success['name']    =   $rider->name;
    
            return response()->json(['success'=>$success], $this->successStatus);
        }
        return response()->json(['error'=>'Contact Number not exist'], $this->errorStatus);
    }

    /** 
     * login Through Api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request)
    { 
        $validator = Validator::make($request->all(),[ 
            'contact'       =>  'required',
            'password'      =>  'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        
        $rider = RiderDetail::where('contact',$request->contact)->first();

        if($rider)
        { 
            if(Hash::check($request->password, $rider['password']))
            {
                if($rider->status == 0)
                {
                    return response()->json(['failed' => 'Rider is not active'], $this->errorStatus);
                }
                else{
                    $rider->update(['device_token'=>$request->device_token]);
                    $success['token'] =  $rider->createToken('MyApp')->accessToken; 
                    return response()->json(['success' => $success], $this->successStatus);
                }
            }
            return response()->json(['error'=>'Invalid password'], $this->errorStatus); 
        } 
        else{ 
            return response()->json(['error'=>'This Phone does not exist'], $this->errorStatus); 
        } 
    }

     /** 
     * Details of Specified Resource 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details(Request $request) 
    { 
        $user = $request->user();
        return response()->json(['success' => $user], $this->successStatus); 
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssignedOrders(Request $request)
    {
        $assign_orders = RiderAssignOrder::with('order')->where('rider_id',$request->Id)->get();
        return result($assign_orders, $this->successStatus, "Rider Assigned Orders.");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectionStatus(Request $request)
    {
        $status = 409;
        $message = 'Oops! Something went wrong.';
        $item = RiderAssignOrder::where('technosys_order_no',$request->orderNo)->where('rider_id',$request->rider_id)->first();

        $admin   = User::first();
        $message = "Rider has been collected an order ".$request->orderNo;
        //  Order Notifiation to Rider
    /*    $notify_detail = [
            'from'      =>  $request->rider_id,
            'to'        =>  $admin->id,
            'message'   =>  $message,
        ];
        Notification::create($notify_detail);
        $notification = new Notification;
        $from       =  'Rehmat-e-Sheeren';
        $notification->toSingleDevice($admin->device_token, $from,$message,null,null);
     */  
        $collect_status = $request->status;
      	//return $item;  
        if ($item->update(['is_collect' => $collect_status,'collect_at'=>now()])) {
            
            $data = 'success';
            $status = 200;  
            $message = 'status updated successfully.';
            
            return result($data, $status, $message);
        }
        return result($data, $status, $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deliveryStatus(Request $request)
    {
	$request->validate([
            'rider_id'  => 'required|exists:rider_details,rider_id',
            'orderNo'   => 'required|exists:rider_assgined_orders,technosys_order_no',
            'status'    => 'required|in:on-way,delivered'
        ]);
        $status = 409;
        $message = 'Oops! Something went wrong.';
        $item = RiderAssignOrder::with('rider')->where('technosys_order_no',$request->orderNo)->where('rider_id',$request->rider_id)->first();
	//return $item;
        $delivery_status = $request->status;
        $admin   = User::first();
        $message = "You Order is arrived its ".$request->status.' for further detail call rider at '.$item->rider->contact;
        //  Order Notifiation to Rider
        $notify_detail = [
            'from'      =>  $admin->id,
            'to'        =>  $item->rider->id,
            'message'   =>  $message,
        ];
        Notification::create($notify_detail);
        $notification = new Notification;
        $from       =  'Rehmat-e-Sheeren';
        $notification->toSingleDevice($item->rider->device_token, $from,$message,null,null);
        if ($item->update(['delivery_status' => $delivery_status,'delivered_at'=>now()])) {

            $data    = 'success';
            $status  = 200;
            $message = 'Delivery status updated successfully.';
            
            return result($data, $status, $message);
        }
        return result($data, $status, $message);
    }
}

