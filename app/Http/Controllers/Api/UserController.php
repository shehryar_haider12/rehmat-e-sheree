<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebUser;
use App\Models\RiderDetail;
use Validator;
use App\Models\OrderDetail;
use Auth;
use Carbon\Carbon;
use Crypt;
use Hash;

class UserController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    /**
     * Store Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'name'                      =>  'required|max:255',
            'phone'                     =>  'required|max:11|unique:web_users,phone',
            'password'                  =>  'required|confirmed',
            'password_confirmation'     =>  'required',
            'loyalty_code'              =>  'required',
            'device_token'              =>  'required',
            'client_information_id'     =>  'required',
//            'card_number'               =>  'required',
  //          'card_issue_date'           =>  'required',
    //        'card_expiry_date'          =>  'required'
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        
	$input              =   $request->all(); 
	//$input['card_issue_date']   =   Carbon::createFromFormat('d-m-Y', $request->card_issue_date)->format('Y-m-d');
       // $input['card_expiry_date']  =   Carbon::createFromFormat('d-m-Y', $request->card_expiry_date)->format('Y-m-d');
	$input['card_expiry_date'] = '2021-02-12';
 	$input['card_issue_date'] = '2021-02-10';
	$input['card_number'] = '5465464';	
	$input['password']  =   Hash::make($input['password']); 
        $input['user_type'] =   0; 
        $user               =   WebUser::create($input); 
        $success['token']   =   $user->createToken('MyApp')->accessToken; 
        $success['name']    =   $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * Store Api Data of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function riderStore(Request $request)
    {   
        $validator = Validator::make($request->all(),[ 
            'contact'                   =>  'required|max:15',
            'password'                  =>  'required|confirmed',
            'password_confirmation'     =>  'required',
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        $rider = RiderDetail::where('contact',$request->contact)->first();
        if(!empty($rider)){
            $input              =   $request->all(); 
            $password           =   Hash::make($input['password']); 
            $rider->update(['password'=>$password,'device_token'=>$request->device_token]); 
            $success['token']   =   $rider->createToken('MyApp')->accessToken; 
            $success['name']    =   $rider->name;
    
            return response()->json(['success'=>$success], $this->successStatus);
        }
        return response()->json(['error'=>'Contact Number not exist'], $this->errorStatus);
    }

    /** 
     * login Through Api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function riderLogin(Request $request)
    { 
        $validator = Validator::make($request->all(),[
            'contact'       =>  'required',
            'password'      =>  'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        
        $rider = RiderDetail::where('contact',$request->contact)->first();

        if($rider)
        { 
            if(Hash::check($request->password, $rider['password']))
            {
                if($rider->status == 0)
                {
                    return response()->json(['failed' => 'Rider is not active'], $this->errorStatus);
                }
                else{
                    $rider->update(['device_token'=>$request->device_token]);
                    $success['token'] =  $rider->createToken('MyApp')->accessToken; 
                    return response()->json(['success' => $success], $this->successStatus);
                }
            }
            return response()->json(['error'=>'Invalid password'], $this->errorStatus); 
        } 
        else{ 
            return response()->json(['error'=>'This Phone does not exist'], $this->errorStatus); 
        } 
    }

    /** 
     * login Through Api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'phone'     =>  'required|max:11',
            'password'  =>  'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
        
        $user = WebUser::where('phone',$request->phone)->first();

        if($user)
        {
            if(Hash::check($request->password, $user['password']))
            {
                if($user->status == 0)
                {
                    return response()->json(['failed' => 'User not active'], $this->errorStatus);
                }
                else{
                    // dd($user);
                    $user->update(['device_token'=>$request->device_token]);
                    $success['token'] =  $user->createToken('MyApp')->accessToken; 
                    return response()->json(['success' => $success], $this->successStatus);
                }
            }
            return response()->json(['error'=>'Invalid password'], $this->errorStatus); 
        } 
        else{ 
            return response()->json(['error'=>'This phone does not exist'], $this->errorStatus); 
        } 
    }

    /** 
     * Details of Specified Resource 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details(Request $request) 
    { 
        $user                   = $request->user();
        $user['total_orders']   = OrderDetail::where('user_id',$user->id)->count();
        return response()->json(['success' => $user], $this->successStatus); 
    } 

     /** 
     * Details of Specified Resource 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function riderDetails(Request $request) 
    { 
        $user = $request->user();
        return response()->json(['success' => $user], $this->successStatus); 
    } 
}

