<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Discount;

class SiteController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllSliders()
    {
        $slider = Slider::where('status',1)->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        return result($slider, $this->successStatus, "Sliders.");
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllDiscounts()
    {
        $discount = Discount::where('status',1)->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        return result($discount, $this->successStatus, "Discounts.");
    }
}
