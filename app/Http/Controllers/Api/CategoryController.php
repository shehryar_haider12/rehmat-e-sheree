<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $categories = Category::where('status',1)->whereNull('deleted_at')->orderBy('sort_by', 'desc')->get();
        return result($categories, $this->successStatus, "Category.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllFeatured()
    {
        $categories = Category::where([['status',1],['is_featured',1]])->whereNull('deleted_at')->orderBy('sort_by', 'asc')->get();
        return result($categories, $this->successStatus, "Featured Category.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMainScreenCategory()
    {
        $categories = Category::where('status',1)->whereNull('deleted_at')->where('is_visible', 1)->orderBy('sort_by', 'asc')->get();
        return result($categories, $this->successStatus, "Main Screen Categories.");
    }
}
