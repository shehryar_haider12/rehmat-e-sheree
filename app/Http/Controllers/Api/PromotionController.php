<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promotion;

class PromotionController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $promotion = Promotion::where('status',1)->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();
        return result($promotion, $this->successStatus, "Promotions.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMainScreenPromotion()
    {
        $promotion = Promotion::where('status',1)->whereNull('deleted_at')->orderBy('created_at', 'desc')->take(3)->get();
        return result($promotion, $this->successStatus, "Fetch Specified Promotions.");
    }
}
