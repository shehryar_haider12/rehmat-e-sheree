<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\WebUser;
use App\Models\CustomerPasswordReset;
use Illuminate\Support\Str;
use Validator;
use Hash;

class PasswordResetController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

   /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = WebUser::where('email', $request->email)->first();
        if (!$user)
        {
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.'
            ], 404);
        }
        $passwordReset = CustomerPasswordReset::updateOrCreate(
            ['id' => $user->id],
            [
                'email' => $user->email,
                'token' => Str::random(32),
            ]
        );
        if ($user && $passwordReset)
        {
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );      
            return response()->json([
                'message' => 'We have e-mailed your password reset link!'
            ]);
        }
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = CustomerPasswordReset::where('token', $token)->first();
        if (!$passwordReset)
        {
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(72)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    } 

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'email'                 => 'required|email',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required',
            'token'                 => 'required|string'
        ]);
        if ($validator->fails()) 
        { 
            return response()->json(['error'=>$validator->errors()], $this->errorStatus);            
        }
   
        $passwordReset = CustomerPasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = WebUser::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.'
            ], 404);
            
        $user->password = Hash::make($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json($user);
    }
}

