<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Category;
use App\Models\CategoryProduct;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories.category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $categories = DB::table('categories')->whereNull('deleted_at')->orderBy('sort_by','desc')->select(['id','image','name', 'status']);

        return DataTables::of($categories)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    => false,
        ];
        return view('categories.add-category',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|unique:categories,name|max:255',
            'image'     =>  'required|mimes:png,jpg,jpeg|max:3000',
            'sort_by'   =>  'required',
        ]);
        $data = $request->all();
        if($request->image)
        {
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        Category::create($data);
        return redirect()->route('categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data = [
            'isEdit'    =>  true,
            'category'  =>  $category
        ];
        return view('categories.add-category',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
	    //dd($request->image);
        $request->validate([
            'name'      =>  'required|max:255|unique:categories,name,'.$category->id,
	    'sort_by'   =>  'required',
	    'image'	=>	'required|max:3000'
        ]);
        $data = $request->all();
        $data['is_visible'] = isset($request->is_visible) ? $request->is_visible : 0;
        $data['is_featured'] = isset($request->is_featured) ? $request->is_featured : 0;
        if($request->image)
        {
            if(!empty($category->image))
            {
                Storage::disk('uploads')->delete($category->image);
            }
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $category->update($data);
        return redirect()->route('categories');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Category::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Category status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $category = Category::findOrFail($request->id);
        $category_assign = CategoryProduct::where('category_id',$request->id)->count();
        // apply your conditional check here
        if ( $category_assign > 0 ) {
            $response['error'] = 'This Category has something assigned to it.';
            return response()->json($response, 409);
        } else {
            // Storage::disk('uploads')->delete($category->image);
            $response = $category->delete();
            return response()->json($response, 200);
        }
    }
}
