<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Discount;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('discounts.discount');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $discounts = DB::table('discounts')->whereNull('deleted_at')->select(['id','text', 'image', 'status']);

        return DataTables::of($discounts)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    => false,
        ];
        return view('discounts.add-discount',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        Discount::create($data);
        return redirect()->route('discounts');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        $data = [
            'isEdit'    =>  true,
            'discount'    =>  $discount
        ];
        return view('discounts.add-discount',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount $discount)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            Storage::disk('uploads')->delete($discount->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $discount->update($data);
        return redirect()->route('discounts');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Discount::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $discount = Discount::findOrFail($request->id);
        // apply your conditional check here
        if ( $discount->delete() ) {
            Storage::disk('uploads')->delete($discount->image);
            $response['success'] = 'Discount Successfully Deleted';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!.';
            return response()->json($response, 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(Discount $discount)
    {
        $discount->update(['image'=>null]);
        Storage::disk('uploads')->delete($discount->image);
        return redirect()->back();
         
    }
}
