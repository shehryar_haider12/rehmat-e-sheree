<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use App\Models\WebUser;
use App\User;
use App\Models\Notification;
use Auth;
use Storage;
use Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        // CategoryProduct::truncate();
        // $client = new \GuzzleHttp\Client();

        // $URI = "https://tsminingapi.azurewebsites.net/TSBE/BranchWiseInventory/GetBranchWiseInventory?AppKey=RS&CompanyBranchIds=1&Barcode=&LoginId=superuser&Password=techSupport20177";
        
        // $response = $client->post($URI, [
        //     'headers'       => [
        //     'Content-Type'  => 'application/json',
        //     ]
        // ]);

        // $technosys_data = $response->getBody();
        // $products = json_decode($technosys_data);
       
        // foreach($products as $product)
        // {
        //     $data = [
        //         'product_id'    =>  $product->ProductItemId,
        //         'category_id'   =>  $product->ProductCategoryId           
        //     ];  
        //     CategoryProduct::create($data);
        // }
        
        // return "asdfasdfasdf";   
        $data = [
            
            'total_products'    => Product::count(),
            'total_categories'  => Category::count(),
            'total_orders'      => OrderDetail::count(),
            'total_customers'   => WebUser::count(),
        ];

        return view('dashboard',$data);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $data = [
            'user' => Auth::user(),
        ];

        return view('settings', $data);
    }
    public function patch(Request $request)
    {
        $request->validate([
            'name'              => 'required|max:191',
            'email'             => "required|max:191|unique:users,email,".Auth::user()->id,
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:6|max:22',
            'avatar'            => 'nullable|image|max:3000',
        ]);

        if($request->change_password == 1 && !Hash::check($request->current_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        $input_data = $request->except('password', 'password_confirmation','_token','_method','change_password','current_password');

        if($request->password)
        {
            $input_data['password'] = bcrypt($request->input('password'));
        }

        if($request->avatar)
        {
            Storage::disk('uploads')->delete(Auth::user()->avatar);
            $input_data['avatar'] = Storage::disk('uploads')->putFile('', $request->avatar);
        }

        Auth::user()->update($input_data);

        return redirect()->route('dashboard');
    }
}
