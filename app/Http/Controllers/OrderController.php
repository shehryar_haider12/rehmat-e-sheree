<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\RiderAssignOrder;
use DataTables;
use PDF;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orders.order');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $orders = OrderDetail::with('orders.product','orderDelivered')->whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','order_no','technosys_order_no','total_amount','phone','address','date','status','created_at']);
       return DataTables::of($orders)->make();
    }

     /**
     * Display Details of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderDetail($order_no)
    {   
        $detail = OrderDetail::where('order_no',$order_no)->first();
        $data = [
            'order_no'          =>   $detail->order_no,
            'techno_order_no'   =>   $detail->technosys_order_no,
        ];
        return view('orders.order_details',$data);
    }

    /**
     * Display Details of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderDetailDatatable($order_no)
    {
        $orders_details = Order::with('product.Unit')->where('order_no',$order_no)->whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','product_id','order_no','quantity','amount','created_at']);
       return DataTables::of($orders_details)->make();
    }   

    /**
     * Display a Invoice of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderInvoice($order_no)
    {   
        $data = [
            'data' =>   orderDetail::with('orders.product.Unit')->where('order_no',$order_no)->first(),
        ];
        $pdf = PDF::loadView('orders.invoice', $data);
        return $pdf->stream('invoice-'.$order_no.'.pdf');

        // return view('orders.invoice',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $status = 409;
        $message = 'Oops! Something went wrong.';
        $data = 'failed';

        $order_no = $request->orderNo;
        $status = $request->input('status');
        $item = OrderDetail::where('order_no',$order_no)->first();
        if ($item->update(['status' => $status])) {

            $data = 'success';
            $status = 200;
            $message = 'Order status updated successfully.';
            
            return result($data, $status, $message);
        }
        return result($data, $status, $message);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function riderAssignOrder()
    {
        return view('riders.assign-order');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function riderAssignOrderdatatable()
    {
        $orders = RiderAssignOrder::with('rider')->whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','rider_id','technosys_order_no','is_collect','collect_at','delivery_status','delivered_at']);
       return DataTables::of($orders)->make();
    }
}
