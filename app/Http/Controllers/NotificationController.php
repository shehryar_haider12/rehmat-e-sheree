<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Membership;
use App\Models\Notification;
use App\Models\WebUser;
use DataTables;
use Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('notification.notification');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $notifications = Notification::with('toContact')->select(['id','to', 'message','read_at', 'created_at']);
        return DataTables::of($notifications)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'customers'    =>  WebUser::get(),
        ];
        return view('notification.add-notification',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Notification $notification)
    {
        $data = [
            'notification'    =>  $notification,
        ];
        return view('notification.view-notification',$data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'message'   =>  'required',
            'to'        =>  'required',
        ]);
        
        $notification = new Notification;
        $user = WebUser::whereIn('id',$request->to)->get();
        // return $user;
        foreach($request->to as $key => $item)
        {
            $notification->to       = $item;
            $notification->from     = Auth::user()->id;
            $notification->message  = $request->message;
            $notification->save();
        }
        $from = 'Rehmat-e-Sheeren';
        // return $from;
        $notification->toMultiDevice($user, $from,$request->message,null,null);
        // Membership::create($request->all());
        return redirect()->route('notification');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend($id)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        
        $notification = Notification::with('toContact')->find($id);
        // $from = Auth::user()->select(['name'])->first();
        $from = 'Rehmat-e-Sheeren';
        $notify = new Notification;

        // dd();
        $token = $notification->toContact->device_token;
        // return $notification->toContact->device_token;
        $notify->toSingleDevice($token, $from, $notification->message, null,null);
        
        
        if ($notification->update(['resend' => $notification->resend + 1,'read_at'=>null])) {
            
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $item = Notification::where('to',$id)->update(['read_at'=>now()]);
        // return $item;
        if ($item) {

            $response['status'] = true;
            $response['message'] = 'Notification Read successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
