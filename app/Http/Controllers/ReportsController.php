<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use PDF;
class ReportsController extends Controller
{
    /**
     * returns the cases report search view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reports.report');
    }

    /**
     * looks up the credentials inside the database and returns data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request)
    {
        $request->validate([
            'nature'        =>  'required',
            'start_date'    =>  'required',
            'end_date'      =>  'required',
        ]);

        $order_data = OrderDetail::with('orders.product','user')->where([
            ['date', '>=', $request->start_date],
            ['date', '<=', $request->end_date],
        ])->get();
        // return $order_data;
        $total_amount   =  $order_data->pluck('total_amount')->toArray();
        $data = [
            'order_data'    =>  $order_data,
            'grand_total'   =>  array_sum($total_amount),
        ];

        $data['start_date'] =   $request->start_date;
        $data['end_date']   =   $request->end_date;
        $data['nature']     =   'SALE WISE REPORT';
        $pdf = \PDF::loadView('reports.report-list', $data);
        $pdf->setPaper('DEFAULT_PDF_PAPER_SIZE', 'portrait');
        return $pdf->stream('report-from-'.$request->start_date.'-to-'.$request->start_date.'.pdf');
        // return view('reports.report-list',$data);
    }   
}
