<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Category;
use App\Models\Transaction;
use App\Models\CategoryProduct;

class PaymentController extends Controller
{
    public function confirm(Request $request)
    {
        $data = $request->all();
        $date= \Carbon\Carbon::parse($data['auth_time'])->format('Y-m-d'); 
        // return $data['decision'];
        if($data['reason_code'] != 100){
            if($data['reason_code'] == 481){
                $message =  "Dear Customer, Your card transaction has been received and under review,
                our representative will contact you shortly for further process.
                ";
            }
            else{
                $message = "Dear Customer, Your card transaction has been declined/reject by your
                card issuing bank, Kindly contact your card issuing bank for details.";
            }
            return response()->json([
                "code"      =>  409,
                "message"   =>  $message,
            ], 409);
        }
        $items = [
            'auth_trans_ref_no'     =>  $data['auth_trans_ref_no'],
            'transaction_id'        =>  $data['transaction_id'],
            'card_type'             =>  $data['card_type_name'],
            'card_num'              =>  $data['req_card_number'],
            'card_expiry'           =>  $data['req_card_expiry_date'],
            'amount'                =>  $data['req_amount'],
            'payment_status'        =>  $data['decision'],
            'transaction_time'      =>  $date,
        ];
        Transaction::create($items);
        return response()->json([
            "code"      =>  200,
            "message"   =>  "Dear Customer, Thank you for your payment, Your transaction has been
            received successfully and your account/order will be updated
            accordingly.",
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactionAll()
    {
        return view('transaction.transaction');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transactionDatatable()
    {
        $transaction = DB::table('transaction')->select(['auth_trans_ref_no','transaction_id','card_type','card_num','card_expiry','amount','payment_status','transaction_time','created_at']);

        return DataTables::of($transaction)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $transaction = Transaction::orderBy('created_at','desc')->get();
      //  $transaction['image_url'] = url('')."/uploads/";

        return result($transaction, 200, "All Transactions.");
    }
}
