<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Promotion;
class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('promotions.promotion');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $promotions = DB::table('promotions')->whereNull('deleted_at')->select(['id','text', 'image', 'status']);

        return DataTables::of($promotions)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    => false,
        ];
        return view('promotions.add-promotion',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        Promotion::create($data);
        return redirect()->route('promotions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        $data = [
            'isEdit'    =>  true,
            'promotion' =>  $promotion
        ];
        return view('promotions.add-promotion',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            Storage::disk('uploads')->delete($promotion->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $promotion->update($data);
        return redirect()->route('promotions');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Promotion::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $promotion = Promotion::findOrFail($request->id);
        // apply your conditional check here
        if ( $promotion->delete() ) {
            Storage::disk('uploads')->delete($promotion->image);
            $response['success'] = 'Promotion Successfully Deleted';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!.';
            return response()->json($response, 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(Promotion $promotion)
    {
        $promotion->update(['image'=>null]);
        Storage::disk('uploads')->delete($promotion->image);
        return redirect()->back();
         
    }
}
