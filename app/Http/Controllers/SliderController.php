<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sliders.slider');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $sliders = DB::table('sliders')->whereNull('deleted_at')->select(['id','text', 'image', 'status']);

        return DataTables::of($sliders)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    => false,
        ];
        return view('sliders.add-slider',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        Slider::create($data);
        return redirect()->route('sliders');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        $data = [
            'isEdit'    =>  true,
            'slider'    =>  $slider
        ];
        return view('sliders.add-slider',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'text'  =>  'nullable|max:100',
            'image' =>  'nullable|mimes:png,jpg,jpeg|max:3000',
        ]);
        $data = $request->all();
        if($request->image)
        {
            Storage::disk('uploads')->delete($slider->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $slider->update($data);
        return redirect()->route('sliders');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Slider::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $slider = Slider::findOrFail($request->id);
        // apply your conditional check here
        if ( $slider->delete() ) {
            Storage::disk('uploads')->delete($slider->image);
            $response['success'] = 'slider Successfully Deleted';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!.';
            return response()->json($response, 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(Slider $slider)
    {
        $slider->update(['image'=>null]);
        Storage::disk('uploads')->delete($slider->image);
        return redirect()->back();
         
    }
}
