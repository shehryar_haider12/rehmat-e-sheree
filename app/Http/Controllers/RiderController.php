<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\RiderDetail;

class RiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('riders.riders');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $riders = RiderDetail::with(['orders'=>function($q){
            $q->count();
        }])->whereNull('deleted_at')->select(['id','rider_id','name', 'contact','branch','duty_timings', 'status'])->get();

        return DataTables::of($riders)->make();
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'    => false,
        ];
        return view('riders.add-rider',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'            =>  'required',
            'contact'         =>  'required|unique:riders,contact',
            'duty_timings'    =>  'required',
            'branch'          =>  'required',
            'password'        =>  'required|confirmed',
        ]);
        $data['password'] = Hash::make($request->password);
        RiderDetail::create($data);
        return redirect()->route('riders');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RiderDetail $rider)
    {
        $data = [
            'isEdit'    =>  true,
            'rider'     =>  $rider
        ];
        return view('riders.add-rider',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiderDetail $rider)
    {
	    //dd($request->image);
        $data = $request->validate([
            'name'            =>  'required',
            'contact'         =>  'required|unique:riders,contact',
            'duty_timings'    =>  'required',
            'branch'          =>  'required',
            'password'        =>  'required|confirmed',
        ]);
        $data['password'] = Hash::make($request->password);
        $rider->update($data);
        return redirect()->route('riders');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = RiderDetail::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $rider = RiderDetail::findOrFail($request->id);
        // apply your conditional check here
        if ( $rider->delete() ) {
            $response['error'] = 'This Rider has something assigned to it.';
            return response()->json($response, 409);
        } else {
            // Storage::disk('uploads')->delete($category->image);
            $response = $category->delete();
            return response()->json($response, 200);
        }
    }
}
