<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
class SiteSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'setting' => Setting::get(),
        ];

        return view('site-settings', $data);
    }
    public function store(Request $request)
    {
        $input = $request->except('_token');
        if(empty($request->checkbox))
        {   
            Setting::where('key','free_delievery_charges_after')->delete();
        }
        foreach ($input as $key => $value) {
            $setting = Setting::where('key',$key)->first();
            if(empty($setting))
            {
                $setting = new Setting;
                $setting->key = $key;   
            }
            $setting->value = $value;
            $setting->save();
        }

        return redirect()->back();
    }
}
