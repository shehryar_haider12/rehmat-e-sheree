<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Storage;
use App\Models\Category;
use App\Models\Product;
use App\Models\CategoryProduct;
use App\Models\Unit;
use App\Models\ProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.product');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $categories = Product::with('categoryProduct.category','unit')->whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','unit_id','featured_image','code','name','price','weight','description','status']);

        return DataTables::of($categories)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'isEdit'        =>  false,
            'categories'    =>  Category::where('status',1)->whereNull('deleted_at')->get(),
            'units'         =>  Unit::where('status',1)->whereNull('deleted_at')->get(),

        ];
        return view('products.add-product',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id'       =>  'nullable',
            'name'              =>  'nullable|max:255',
            'code'              =>  'nullable|max:255|unique:products,code',
            'price'             =>  'nullable|max:255',
            'weight'            =>  'nullable|max:255',
            'description'       =>  'nullable',
            'featured_image'    =>  'required|mimes:png,jpg,jpeg|max:3000',
            ]);
        $data = $request->all();
        if($request->featured_image)
        {
            $data['featured_image'] = Storage::disk('uploads')->putFile('',$request->featured_image);
        }
        $product = Product::create($data);
        foreach($data['category_id'] as $category)
        {
            $category_product = new CategoryProduct;
            $category_product->product_id  = $product->id;
            $category_product->category_id = $category;
            $category_product->save();
        }
        if($request->images)
        {
            foreach($data['images'] as $value)
            {
                $product_images             =   new ProductImage;
                $image  = Storage::disk('uploads')->putFile('',$value);
                $product_images->product_id =   $product->id; 
                $product_images->images     =   $image; 
                $product_images->save();
            }
        }
        return redirect()->route('products');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data = [
            'isEdit'        =>  true,
            'categories'    =>  Category::where('status',1)->whereNull('deleted_at')->get(),
            'units'         =>  Unit::where('status',1)->whereNull('deleted_at')->get(),
            'product'       =>  $product,
        ];
        return view('products.add-product',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'category_id'       =>  'nullable',
            'name'              =>  'nullable|max:255',
            'code'              =>  'nullable|max:255|unique:products,code,'.$product->id,
            'price'             =>  'nullable|max:255',
            'weight'            =>  'nullable|max:255',
            'description'       =>  'nullable',
        ]);
        $data           = $request->all();
        $data['is_featured']       = isset($request->is_featured) ? $request->is_featured : 0;
        $data['customize_box']  = isset($request->customize_box) ? $request->customize_box : 0;

        if($request->featured_image)
        {
            $request->validate([
                'featured_image' =>  'required|mimes:png,jpg,jpeg|max:3000'
            ]);
            Storage::disk('uploads')->delete($product->featured_image);
            $data['featured_image'] = Storage::disk('uploads')->putFile('',$request->featured_image);
        }
        // return $data;
        $product->update($data);
        
        CategoryProduct::where('product_id',$product->id)->delete();

        if($request->images)
        {
            foreach($data['images'] as $value)
            {
                $product_images     =   new ProductImage;
                $image  = Storage::disk('uploads')->putFile('',$value);
                $product_images->product_id =   $product->id; 
                $product_images->images     =   $image; 
                $product_images->save();
            }
        }

        foreach($data['category_id'] as $category)
        {
            $category_product = new CategoryProduct;
            $category_product->product_id  = $product->id;
            $category_product->category_id = $category;
            $category_product->save();
        }
        return redirect()->route('products');
    }

    /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Product::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Product status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $product = Product::findOrFail($request->id);
        CategoryProduct::where('product_id',$product->id)->delete();
        // $images = ProductImage::where('product_id',$product->id)->pluck('images');
        // foreach($images as $item)
        // {
        //     Storage::disk('uploads')->delete($item);
        // }
        ProductImage::where('product_id',$product->id)->delete();
        // apply your conditional check here
        if ( false ) {
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        } else {
            // Storage::disk('uploads')->delete($product->featured_image);
            $response = $product->delete();
            return response()->json($response, 200);
        }
    }

         /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProductImage(ProductImage $product_image)
    {
        Storage::disk('uploads')->delete($product_image->images);
        $product_image->delete();
        return redirect()->back();    
    }
}
