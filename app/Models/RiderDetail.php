<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Laravel\Passport\HasApiTokens;

class RiderDetail extends Authenticatable
{
    use HasApiTokens, SoftDeletes;

    protected $table = 'rider_details';
    public $fillable = ['rider_id','name','contact','password','status','device_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\RiderAssignOrder', 'rider_id', 'rider_id');
    }
}
