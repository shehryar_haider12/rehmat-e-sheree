<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    protected $table = 'order_details';
    protected $fillable = ['order_no','technosys_order_no','client_order_id','user_id', 'order_deliver_type_id','payment_mode_id', 'remarks','delievery_charges','total_amount','phone','address','address_lat','address_long','status','date'];

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'order_no', 'order_no');
    }

    public function user()
    {
        return $this->hasOne('App\Models\WebUser', 'id', 'user_id');
    }

    public function orderDelivered()
    {
        return $this->hasMany('App\Models\RiderAssignOrder', 'technosys_order_no', 'technosys_order_no');
    }
}
