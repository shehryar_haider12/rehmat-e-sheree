<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{

    protected $table = 'category_products';
    protected $fillable = ['product_id','category_id'];    
    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function products()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}
