<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    public $fillable = ['id','unit_id','name','code','featured_image','price','weight','is_featured','description','created_by','updated_by','customize_box','status'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    //    static::save(function($model)
    //    {
    //        $user = Auth::user();
    //        $model->updated_by = $user->id;
    //    });
   }

    public function categoryProduct()
    {
        return $this->hasMany('App\Models\CategoryProduct', 'product_id', 'id');
    }

    public function productImages()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id');
    }
    public function Unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }
}
