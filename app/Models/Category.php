<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    public $fillable = ['id','name','image','is_visible','is_featured','sort_by','created_by','updated_by','status'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    //    static::save(function($model)
    //    {
    //        $user = Auth::user();
    //        $model->updated_by = $user->id;
    //    });
   }

    public function categoryProduct()
    {
        return $this->hasMany('App\Models\CategoryProduct', 'category_id', 'id');
    }
}
