<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPasswordReset extends Model
{
    use SoftDeletes;

    const UPDATED_AT = null;
    protected $table = 'customer_password_resets';
    public $fillable = ['token','email'];
    // public $primaryKey = 'email';
}
