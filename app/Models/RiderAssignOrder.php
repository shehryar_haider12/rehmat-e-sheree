<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiderAssignOrder extends Model
{
    use SoftDeletes;
    protected $table = 'rider_assgined_orders';
    public $fillable = ['rider_id','technosys_order_no','is_collect', 'delivered_at','delivery_status','error'];

    public function rider()
    {
        return $this->hasOne('App\Models\RiderDetail', 'rider_id', 'rider_id');
    }

    public function order()
    {
        return $this->hasOne('App\Models\OrderDetail', 'technosys_order_no', 'technosys_order_no');
    }
}
