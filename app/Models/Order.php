<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';
    public $fillable = ['product_id','order_no','quantity','amount'];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }


}
