<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Unit extends Model
{
    use SoftDeletes;

    protected $table = 'units';
    public $fillable = ['name','status','created_by','updated_by'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    //    static::save(function($model)
    //    {
    //        $user = Auth::user();
    //        $model->updated_by = $user->id;
    //    });
   }
}
