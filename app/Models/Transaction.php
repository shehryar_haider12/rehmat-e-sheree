<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = [
        'auth_trans_ref_no',
        'transaction_id',
        'card_type',
        'card_num',
        'card_expiry',
        'amount',
        'payment_status',
        'transaction_time'
    ];
    
}
