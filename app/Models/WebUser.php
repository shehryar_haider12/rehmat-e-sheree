<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;


class WebUser extends Authenticatable
{
    use Notifiable, HasApiTokens,SoftDeletes;

    protected $table = 'web_users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name','phone','client_information_id','password','loyalty_code','device_token','status','card_number','card_issue_date','card_expiry_date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
