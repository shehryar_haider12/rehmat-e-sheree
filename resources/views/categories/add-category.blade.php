@extends('layouts.masterpage')

@section('title', 'Add Category')

@section('top-styles')

@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">
        <div class="btn-group pull-right">
          <button class="btn btn-dark-theme waves-effect waves-light" style="background-color: #dc3535 !important" type="button" onclick="window.history.back(1)"><span class="btn-label"><i class="fa fa-arrow-left"></i></span>Go back</button>
        </div>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{route('categories')}}">Category</a>
          </li>
          <li class="breadcrumb-item active">Add Category</li>
        </ol>

      </div>
    </div>

    <form action="{{$isEdit ? route('category.update',$category->id) : route('category.store')}}" method="POST" enctype="multipart/form-data">
      @csrf
      @if ($isEdit)
        <input type="hidden" name="_method" value="put">
      @endif
      <div class="portlet">
        <div class="portlet-heading bg-light-theme">
          <h3 class="portlet-title">
            <span class="ti-user mr-2"></span>Add Category</h3>
          <div class="portlet-widgets">
            <span class="divider"></span>
            <button type="submit" class="btn btn-white waves-effect btn-rounded">
              <span class="btn-label">
                <i class="fa fa-save"></i>
              </span> Save
            </button>
          </div>
          <div class="clearfix"></div>
        </div>

        <div id="bg-inverse" class="panel-collapse collapse show" style="">
          <div class="portlet-body">

            <div class="card-box">
              <div class="section">
              <div class="row">
                <div class="col-md-6">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' name="image" id="imageUpload1" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload1"><span>Featured Image  </span></label>
                    </div>
                    <div class="avatar-preview">
                      <div id="imagePreview1" style="background-image : url({{url('').'/uploads/'}}{{$category->image ?? 'placeholder.jpg'}}">
                      </div>
                    </div>
                  </div>
                  <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                </div>
                <div class="col-md-6">
                  <div class="row">
                  <div class="form-group col-md-6">
                    <label>Name
                      <span class="text-danger">*</span>
                    </label>
                    <input type="text" name="name" parsley-trigger="change" required placeholder="Name..." class="form-control" id="name" value="{{$category->name ?? null }}">
                    <span class="text-danger">{{$errors->first('name')?? null}}</span>
                  </div>
                  <div class="form-group col-md-6">
                    <div class="mt30">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="changePasswordCheckbox" name="is_featured" @if($isEdit){{ $category->is_featured == 1 ? 'checked' : null }}@endif value="1">
                            <label class="custom-control-label" for="changePasswordCheckbox">Is Featured</label>
                        </div>
                    </div>
                  </div>
                </div>
                  <div class="row">
                  <div class="form-group col-md-6">
                    <label>Sort
                      <span class="text-danger">*</span>
                    </label>
                    <input type="number" min="0" name="sort_by" parsley-trigger="change" required placeholder="Sort..." class="form-control" id="sort_by" value="{{$category->sort_by ?? null }}">
                    <span class="text-danger">{{$errors->first('sort_by')?? null}}</span>
                  </div>
                  <div class="form-group col-md-6">
                    <div class="mt30">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="is_visible" name="is_visible" @if($isEdit) {{ $category->is_visible == 1 ? 'checked' : null }} @endif   value="1">
                        <label class="custom-control-label" for="is_visible">Visible on app main screen</label>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </form>

  </div>
  <!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
  @parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
  jQuery(document).ready(function () {

    $('form').parsley();
    $('.select2').select2();

    function readURL(input, number) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
          $('#imagePreview' + number).hide();
          $('#imagePreview' + number).fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#imageUpload1").change(function () {
      readURL(this, 1);
    });


  });
</script>
@endsection

