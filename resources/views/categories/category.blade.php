  @extends('layouts.masterpage')

@section('title', 'Categories')

@section('top-styles')
<!-- Plugins css-->
<link href="{{url('')}}/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

<!-- DataTables -->
<link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<style>
  a.btn.btn-secondary {
    background: #dc3535bf;
    border: 1px solid white;
}
</style>
@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <!-- <h4 class="page-title">Portlets</h4> -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item active">Categories</li>
        </ol>
      </div>
    </div>

    <div class="portlet">
      <div class="portlet-heading bg-light-theme">
        <h3 class="portlet-title">
          <i class="ti-sharethis mr-2"></i> Categories</h3>
        <div class="portlet-widgets">
            <a href="{{route('category.create')}}">
              <button class="btn btn-white btn-custom-white btn-custom btn-rounded waves-effect" type="button">
                <i class="fa fa-plus"></i> Add Category</button>
            </a>
        </div>
        <div class="clearfix"></div>
      </div>
      <div id="bg-primary1" class="panel-collapse collapse show">
        <div class="portlet-body">
          <div class="custom_datatable">
            <form action="#" id="advanceSearch">
              <div class="bg-black-transparent1 m-b-15 p15 pb0">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Name</label>
                      <input type="text" name="name" id="autocomplete-ajax1" class="form-control" placeholder="Name" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group" style="margin-top: 28px;">
                      <button id="search" class="btn btn-block waves-effect waves-light" style="background: #dc3535db; color: white;">
                        <i class="fa fa-search pr-1"></i> Search</button>
                    </div>
                  </div>
                  {{-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Description</label>
                      <input type="text" name="description" id="autocomplete-ajax1" class="form-control" placeholder="Description" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div> --}}
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-8">
                  </div>
                </div>
              </div>
            </form>
            <table id="datatable" class="table table-bordered table-striped table-responsive" width="100%" cellspacing="0" cellpadding="0">
              <thead>
                {{-- 'id','field_type','name','contact_person','telephone','mobile','cnic','email','region','sub_region','address','credit_limit','credit_terms','remarks','st_reg_no','website','fax', 'status' --}}
                <tr>
                  <th class="no-sort text-center" width="5%">S.No</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th class="no-sort text-center" width="10%">Actions</th>
                </tr>
              </thead>
            </table>

          </div>
        </div>
      </div>

    </div>
    <!-- end row -->


  </div>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
  @parent
@endsection

@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/switchery/js/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.js"></script>

{{-- PDF MAKE --}}
<script src="{{url('')}}/plugins/datatables/pdfmake.min.js"></script>
<script src="{{url('')}}/plugins/datatables/vfs_fonts.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.html5.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.print.min.js"></script>

@endsection

@section('bottom-bot-scripts')
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable').DataTable({
      processing: true,
      serverSide: true,
      dom: 'Bfrtip',
      buttons: [
            'csv',
            {
              text: 'PDF',
              extend: 'pdfHtml5',
              // filename: 'units',
              // orientation: 'landscape', //portrait
              // download: 'open',
              // pageSize: 'A4', //A3 , A5 , A6 , legal , letter
              exportOptions: {
                columns: [ 0,1],
                search: 'applied',
                order: 'applied'
                },
					customize: function (doc) {
            doc.content[1].table.widths = ['10%','90%'];
            doc.styles.tableBodyEven.alignment = 'center';
            doc.styles.tableBodyOdd.alignment = 'center'; 
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
            doc.pageMargins = [20,60,20,30];
						// Set the font size fot the entire document
						doc.defaultStyle.fontSize = 7;
						// Set the fontsize for the table header
						doc.styles.tableHeader.fontSize = 7;
						// Create a header object with 3 columns
						// Left side: Logo
						// Middle: brandname
						// Right side: A document title
						doc['header']=(function() {
							return {
								columns: [
									{
										alignment: 'left',
										italics: true,
										text: 'Rehmat-e-Sheeren',
										fontSize: 18,
										margin: [10,0]
									},
									{
										alignment: 'right',
										fontSize: 14,
										text: 'Categories'
									}
								],
								margin: 20
							}
						});
						// Create a footer object with 2 columns
						// Left side: report creation date
						// Right side: current page and total pages
						doc['footer']=(function(page, pages) {
							return {
								columns: [
									{
										alignment: 'left',
										text: ['Date: ', { text: jsDate.toString() }]
									},
									{
										alignment: 'right',
										text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
									}
								],
								margin: 20
							}
						});
						// Change dataTable layout (Table styling)
						// To use predefined layouts uncomment the line below and comment the custom lines below
						// doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return .5; };
						objLayout['vLineWidth'] = function(i) { return .5; };
						objLayout['hLineColor'] = function(i) { return '#aaa'; };
						objLayout['vLineColor'] = function(i) { return '#aaa'; };
						objLayout['paddingLeft'] = function(i) { return 4; };
						objLayout['paddingRight'] = function(i) { return 4; };
						doc.content[0].layout = objLayout;
				}
				},
            'print'
        ],
      ajax: '{{route("category.datatable")}}',
      "columns": [
        { "data": "id", "defaultContent": "" },
        { "data": "image", "defaultContent": "" },
        { "data": "name", "defaultContent": "" },
        { "data": "id", "defaultContent": "" },
       ],
      "columnDefs": [{
        "targets": 'no-sort',
        "orderable": false,
      },
      {
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + 1;
        },
      },
      {
        "targets": 1,
        "render": function (data, type, row, meta) {
          return `<img src='{{url('')}}/uploads/`+data+`' height='50px' alt='image'/>`;
        },
      },
      {
        "targets": -1,
        "render": function (data, type, row, meta) {
          var edit = '{{route("category.edit",[":id"])}}';
          edit = edit.replace(':id', data);
          var checked = row.status == 1 ? 'checked' : null;
          return `
          <a href="` + edit + `" class="text-info p-1" data-original-title="Edit" title="" data-placement="top" data-toggle="tooltip">
              <i class="fa fa-pencil"></i>
          </a>
          <a href="javascript:;" class="delete text-danger p-2" data-original-title="Delete" title="" data-placement="top" data-toggle="tooltip" data-id="`+ data + `">
              <i class="fa fa-trash-o"></i>
          </a>
          <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` + checked + ` value="` + row.id + `">
          `;
        },
      },
      ],
      "drawCallback": function (settings) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
        if (elems) {
          elems.forEach(function (html) {
            var switchery = new Switchery(html, {
              color: '#002644'
              , secondaryColor: '#dfdfdf'
              , jackColor: '#fff'
              , jackSecondaryColor: null
              , className: 'switchery'
              , disabled: false
              , disabledOpacity: 0.5
              , speed: '0.1s'
              , size: 'small'
            });

          });
        }

        $('.status').change(function () {
          var $this = $(this);
          var id = $this.val();
          var status = this.checked;

          if (status) {
            status = 1;
          } else {
            status = 0;
          }

          axios
            .post('{{route("category.status")}}', {
              _token: '{{csrf_token()}}',
              _method: 'patch',
              id: id,
              status: status,
            })
            .then(function (responsive) {
              console.log(responsive);
            })
            .catch(function (error) {
              console.log(error);
            });
        });

        $('.delete').click(function () {
          var deleteId = $(this).data('id');
          var $this = $(this);

          swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4fa7f3',
            cancelButtonColor: '#d57171',
            confirmButtonText: 'Yes, delete it!'
          }).then(function (result) {
            if (result.value) {
            axios
              .post('{{route("category.delete")}}', {
                _method: 'delete',
                _token: '{{csrf_token()}}',
                id: deleteId,
              })
              .then(function (response) {
                console.log(response);

                swal(
                  'Deleted!',
                  'Customer has been deleted.',
                  'success'
                )

                table
                  .row($this.parents('tr'))
                  .remove()
                  .draw();
              })
              .catch(function (error) {
                console.log(error);
                swal(
                  'Failed!',
                  error.response.data.error,
                  'error'
                )
              });
            }
          })
        });
      },
      //scrollX:true,
    });
    $('#advanceSearch').submit(function(e){
        e.preventDefault();
        // var date = moment($('input[name="date"]').val(),'YYYY/MM/DD').format('YYYY-MM-DD');
        // if(date === 'Invalid date')
        // {
        //   date = '';
        // }
        table.columns(2).search($('input[name="name"]').val());
        // table.columns(10).search(date);
        // table.columns(11).search($('.ver-status').val());
        table.draw();
      });

      $(".custom_datatable #DataTable_wrapper .row:nth-child(2) .col-sm-12").niceScroll();
  });

</script>
@endsection
