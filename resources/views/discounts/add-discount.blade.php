@extends('layouts.masterpage')

@section('title', 'Add Discount')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/plugins/select2/css/select2.min.css">
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }

</style>
@endsection

@section('header')
@parent
@endsection

@section('leftsidebar')
@parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-dark-theme waves-effect waves-light"
                        style="background-color: #dc3535 !important" type="button"
                        onclick="window.history.back(1)"><span class="btn-label"><i
                                class="fa fa-arrow-left"></i></span>Go back</button>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories')}}">Discount</a>
                    </li>
                    <li class="breadcrumb-item active">Add Discount</li>
                </ol>

            </div>
        </div>

        <form action="{{$isEdit ? route('discount.update',$discount->id) : route('discount.store')}}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @if ($isEdit)
            <input type="hidden" name="_method" value="put">
            @endif
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        <span class="ti-user mr-2"></span>Add Discount</h3>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                        <button type="submit" class="btn btn-white waves-effect btn-rounded">
                            <span class="btn-label">
                                <i class="fa fa-save"></i>
                            </span> Save
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type='file' name="image" id="imageUpload1"
                                                    accept=".png, .jpg, .jpeg" />
                                                <label for="imageUpload1"><span>Featured Image </span></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="imagePreview1"
                                                    style="background-image : url({{url('').'/uploads/'}}{{$discount->image ?? 'placeholder.jpg'}}">
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Banner Text
                                                {{-- <span class="text-danger">*</span> --}}
                                            </label>
                                            <input type="text" name="text" parsley-trigger="change"
                                                placeholder="Banner text..." class="form-control" id="text"
                                                value="{{$discount->text ?? old('text') }}">
                                            <span class="text-danger">{{$errors->first('text')?? null}}</span>
                                        </div>
                                    </div>
                                    @if ($isEdit)
                                    @if (!empty($discount->image))
                                    <div class="images col-md-2">
                                        <a href="{{route('discount.delete-image',$discount->id)}}"><span
                                                class="fa fa-close"></span></a>
                                        <img src="{{url('').'/uploads/'.$discount->image}}" height='121px'
                                            width='190px'>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    </form>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
@parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
    jQuery(document).ready(function () {

        $('form').parsley();
        $('.select2').select2();

        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });

    });

</script>
@endsection
