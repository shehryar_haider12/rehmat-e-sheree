@extends('layouts.masterpage')

@section('title', 'Orders Details')

@section('top-styles')
<!-- Plugins css-->
<link href="{{url('')}}/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

<!-- DataTables -->
<link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <!-- <h4 class="page-title">Portlets</h4> -->
        <div class="btn-group pull-right">
          <a href="{{route('order.invoice',$order_no)}}" target="_blank"><button class="btn btn-dark-theme waves-effect waves-light" style="background-color: #dc3535 !important" type="button"><span class="btn-label"><i class="fa fa-file"></i></span>Generate Invoice</button></a>
        </div>
        <div class="btn-group pull-right">
          <button class="btn btn-dark-theme waves-effect waves-light" style="background-color: #dc3535 !important;border-color:white !important;" type="button" onclick="window.history.back(1)"><span class="btn-label"><i class="fa fa-arrow-left"></i></span>Go back</button>
        </div>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item active">Order Details</li>
        </ol>
      </div>
    </div>

    <div class="portlet">
      <div class="portlet-heading bg-light-theme">
        <h3 class="portlet-title">
          <i class="ti-sharethis mr-2"></i> Order  ({{$techno_order_no}}) Details</h3>
        <div class="clearfix"></div>
      </div>
      <div id="bg-primary1" class="panel-collapse collapse show">
        <div class="portlet-body">
          <div class="custom_datatable">

            <table id="datatable" class="table table-bordered table-striped table-responsive" width="100%" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th class="no-sort text-center" width="5%">S.No</th>
                  <th>Order No</th>
                  <th>Product Image</th>
                  <th>Product Code</th>
                  <th>Product name</th>
                  <th>Product Price</th>
                  <th>Quantity</th>
                  <th>Units</th>
                  <th>Amount</th>
                  {{-- <th class="no-sort text-center" width="10%">Actions</th> --}}
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->
</div>
</div>
<!-- container -->
</div>
<!-- content -->
@endsection
@section('rightsidebar')
  @parent
@endsection
@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/switchery/js/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{route("order.detail.datatable",$order_no)}}',
      "columns": [
        { "data": "id", "defaultContent": "" },
        { "data": "order_no", "defaultContent": "" },
        { "data": "product.featured_image", "defaultContent": "" },
        { "data": "product.code", "defaultContent": "" },
        { "data": "product.name", "defaultContent": "" },
        { "data": "product.price", "defaultContent": "" },
        { "data": "quantity", "defaultContent": "" },
        { "data": "product.unit.name", "defaultContent": "" },
        { "data": "amount", "defaultContent": "" },
       ],
      "columnDefs": [{
        "targets": 'no-sort',
        "orderable": false,
      },
      {
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + 1;
        },
      },
      {
        "targets": 2,
        "render": function (data, type, row, meta) {
          return `<img src='{{url('')}}/uploads/`+data+`' height='50px' alt='image'/>`;
        },
      },
      {
        "targets": -1,
        "render": function (data, type, row, meta) {
          return row.product.price * row.quantity;        },
      },

      ],
      "drawCallback": function (settings) {

      },
      //scrollX:true,
    });
  });

</script>
@endsection
