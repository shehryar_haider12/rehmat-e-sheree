<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Invoice</title>
	<link rel='stylesheet' type='text/css' href='{{url('')}}/assets/css/invoice.css' />
    <style>
        .text-center
        {
            text-align: center;
        }
        p#date {
            text-align: left;
        }
    </style>
</head>

<body>
    <div style="width:800px;">
        <div id="page-wrap">

            <p id="header">INVOICE</p>
            <p class="text-center">Order #{{$data->technosys_order_no}}</p>
            {{-- <br> --}}
            <div id="identity">
            
                <p id="address"  style="font-size: 12px;">  <br> <span style="margin-bottom: 20px">{{$data->address}}</span><br> <span style="margin-bottom: 20px">Phone: {{$data->phone}}</span></p>

                <div id="logo">
                    {{-- <img id="image" src="images/logo.png" alt="logo" /> --}}
                    <h4><span style="color: red">Rehmat</span>-e-<span style="color: red">Sheeren</span></h4>
                </div>
            
            </div>
            <br>
            {{-- <div style="clear:both"></div> --}}
            
            <div id="customer"  style="font-size: 12px;">

                {{-- <p id="customer-title">Widget Corp.<br> c/o Steve Widget</p> --}}

                <table id="meta">
                    <tr>
                        <td class="meta-head">Invoice #</td>
                        <td><p id="date"></p></td>
                    </tr>
                    <tr>

                        <td class="meta-head">Date</td>

                        <td><p id="date">{{date('M d, Y', strtotime($data->date))}}</p></td>
                    </tr>
                </table>
            
            </div>
            
            <table id="items"  style="font-size: 12px;" width='100%'>
            <thead>
            <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th colspan="2">Units</th>
                <th colspan="2">Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data->orders as $item)
                <tr class="item-row">
                <td style="border-right: 1px solid black !important" class="text-center"><div class="delete-wpr"><p>{{$item->product->name}}</p></div></td>
                <td style="border-right: 1px solid black !important"><p class="text-center qty">x{{$item->quantity}}</p></td>
                <td colspan="2" style="border-right: 1px solid black !important"><p class="text-center cost">{{$item->product->Unit->name ??  null}}</p></td>
                <td colspan="2" style="border-right: 1px solid black !important"><p class="text-center price">{{$item->product->price * $item->quantity}}</p></td>
                </tr>
            @endforeach
            <tr id="hiderow">
                <td colspan="6"></td>
            </tr>
            
            <tr>
                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line">Subtotal</td>
                <td colspan="2" class="total-value"><div id="subtotal">{{$data->total_amount - $data->delievery_charges}}</div></td>
            </tr>
            <tr>
                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line balance">Delievery Charges</td>
                <td colspan="2" class="total-value balance"><div class="due">{{$data->delievery_charges}}</div></td>
                </tr>
            <tr>

                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line">Total Amount</td>
                <td colspan="2" class="total-value"><div id="total">{{$data->total_amount}}</div></td>
            </tr>
            </tbody>
            </table>
            
            <div id="terms">
            <h5>Terms</h5>
            {{-- <p>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</p> --}}
            <p>Rehmat-e-Sheeren</p>
            </div>
        
        </div>
        <div id="page-wrap">

            <p id="header">INVOICE</p>
            <p class="text-center">Order #{{$data->technosys_order_no}}</p>
            {{-- <br> --}}
            <div id="identity">
            
                <p id="address"  style="font-size: 12px;"> <br> <span style="margin-bottom: 20px">Address: {{$data->address}}</span><br> <span style="margin-bottom: 20px">Phone: {{$data->phone}}</span></p>

                <div id="logo">
                    {{-- <img id="image" src="images/logo.png" alt="logo" /> --}}
                    <h4><span style="color: red">Rehmat</span>-e-<span style="color: red">Sheeren</span></h4>
                </div>
            
            </div>
            <br>

            {{-- <div style="clear:both"></div> --}}
            
            <div id="customer"  style="font-size: 12px;">

                {{-- <p id="customer-title">Widget Corp.<br> c/o Steve Widget</p> --}}

                <table id="meta">
                    <tr>
                        <td class="meta-head">Invoice #</td>
                        <td><p id="date"></p></td>
                    </tr>
                    <tr>

                        <td class="meta-head">Date</td>

                        <td><p id="date">{{date('M d, Y', strtotime($data->date))}}</p></td>
                    </tr>
                </table>
            
            </div>
            
            <table id="items"  style="font-size: 12px;" width='100%'>
            <thead>
            <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th colspan="2">Units</th>
                <th colspan="2">Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data->orders as $item)
                <tr class="item-row">
                <td style="border-right: 1px solid black !important" class="text-center"><div class="delete-wpr"><p>{{$item->product->name}}</p></div></td>
                <td style="border-right: 1px solid black !important"><p class="text-center qty">x{{$item->quantity}}</p></td>
                <td colspan="2" style="border-right: 1px solid black !important"><p class="text-center cost">{{$item->product->Unit->name ??  null}}</p></td>
                <td colspan="2" style="border-right: 1px solid black !important"><p class="text-center price">{{$item->product->price * $item->quantity}}</p></td>
                </tr>
            @endforeach
            <tr id="hiderow">
                <td colspan="6"></td>
            </tr>
            
            <tr>
                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line">Subtotal</td>
                <td colspan="2" class="total-value"><div id="subtotal">{{$data->total_amount - $data->delievery_charges}}</div></td>
            </tr>
            <tr>
                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line balance">Delievery Charges</td>
                <td colspan="2" class="total-value balance"><div class="due">{{$data->delievery_charges}}</div></td>
                </tr>
            <tr>

                <td colspan="2" class="blank"> </td>
                <td colspan="2" class="total-line">Total Amount</td>
                <td colspan="2" class="total-value"><div id="total">{{$data->total_amount}}</div></td>
            </tr>
            </tbody>
            </table>
            
            <div id="terms">
            <h5>Terms</h5>
            {{-- <p>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</p> --}}
            <p>Rehmat-e-Sheeren</p>
            </div>
        
        </div>
    </div>
</body>

</html>