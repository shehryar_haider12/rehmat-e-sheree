@extends('layouts.masterpage')

@section('title', 'Orders')

@section('top-styles')
<!-- Plugins css-->
<link href="{{url('')}}/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

<!-- DataTables -->
<link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<style>
  a.btn.btn-secondary {
    background: #dc3535bf;
    border: 1px solid white;
}
</style>
@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <!-- <h4 class="page-title">Portlets</h4> -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item active">Orders</li>
        </ol>
      </div>
    </div>

    <div class="portlet">
      <div class="portlet-heading bg-light-theme">
        <h3 class="portlet-title">
          <i class="ti-sharethis mr-2"></i> Orders</h3>
        <div class="clearfix"></div>
      </div>
      <div id="bg-primary1" class="panel-collapse collapse show">
        <div class="portlet-body">
          <div class="custom_datatable">
            <form action="#" id="advanceSearch">
              <div class="bg-black-transparent1 m-b-15 p15 pb0">
                <div class="row">
                  {{-- <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Category</label>
                      <select name="category_product" id="autocomplete-ajax1" class="form-control select2" autocomplete="off">
                        <option value="Desserts">Desserts</option>
                      </select>
                    </div>
                  </div> --}}
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Order No</label>
                      <input type="text" name="order_no" id="autocomplete-ajax1" class="form-control" placeholder="Name" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Amount</label>
                      <input type="text" name="amount" id="autocomplete-ajax1" class="form-control" placeholder="Amount" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">Date</label>
                      <input type="date" name="date" id="autocomplete-ajax1" class="form-control" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div>
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-1">
                  </div>
                  <div class="col-md-3">
                    <div class="form-group" style="margin-top: 28px;">
                      <button id="search" class="btn btn-block waves-effect waves-light" style="background: #dc3535db; color: white;">
                        <i class="fa fa-search pr-1"></i> Search</button>
                    </div>
                  </div>
                  {{-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Description</label>
                      <input type="text" name="description" id="autocomplete-ajax1" class="form-control" placeholder="Description" style=" z-index: 2;" autocomplete="off"/>
                    </div>
                  </div> --}}
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-8">
                  </div>
                </div>
              </div>
            </form>
            <table id="datatable" class="table table-bordered table-striped table-responsive" width="100%" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th class="no-sort text-center" width="5%">S.No</th>
                  <th>Order No</th>
                  <th>Items</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Total Amount</th>
                  <th>Date</th>
                  <th>Deliverey Status</th>
                  <th>View Details</th>
                  <th class="no-sort text-center" width="10%">Actions</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- end row -->
</div>
</div>
<!-- container -->
</div>
<!-- content -->
@endsection
@section('rightsidebar')
  @parent
@endsection
@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/switchery/js/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.js"></script>

{{-- PDF MAKE --}}
<script src="{{url('')}}/plugins/datatables/pdfmake.min.js"></script>
<script src="{{url('')}}/plugins/datatables/vfs_fonts.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.html5.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.print.min.js"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable').DataTable({
      processing: true,
      serverSide: true,
      dom: 'Bfrtip',
      buttons: [
            'csv',
            {
              text: 'PDF',
              extend: 'pdfHtml5',
              // filename: 'units',
              orientation: 'landscape', //portrait
              // download: 'open',
              pageSize: 'A4', //A3 , A5 , A6 , legal , letter
              exportOptions: {
                columns: [ 0,1,3,4,5,6,7],
                search: 'applied',
                order: 'applied'
                },
					customize: function (doc) {
            doc.content[1].table.widths = ['10%','20%','10%','30%','10%','10%','10%'];
            doc.styles.tableBodyEven.alignment = 'center';
            doc.styles.tableBodyOdd.alignment = 'center'; 
						//Remove the title created by datatTables
						doc.content.splice(0,1);
						//Create a date string that we use in the footer. Format is dd-mm-yyyy
						var now = new Date();
						var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
            doc.pageMargins = [20,60,20,30];
						// Set the font size fot the entire document
						doc.defaultStyle.fontSize = 7;
						// Set the fontsize for the table header
						doc.styles.tableHeader.fontSize = 7;
						// Create a header object with 3 columns
						// Left side: Logo
						// Middle: brandname
						// Right side: A document title
						doc['header']=(function() {
							return {
								columns: [
									{
										alignment: 'left',
										italics: true,
										text: 'Rehmat-e-Sheeren',
										fontSize: 18,
										margin: [10,0]
									},
									{
										alignment: 'right',
										fontSize: 14,
										text: 'Products'
									}
								],
								margin: 20
							}
						});
						// Create a footer object with 2 columns
						// Left side: report creation date
						// Right side: current page and total pages
						doc['footer']=(function(page, pages) {
							return {
								columns: [
									{
										alignment: 'left',
										text: ['Date: ', { text: jsDate.toString() }]
									},
									{
										alignment: 'right',
										text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
									}
								],
								margin: 20
							}
						});
						// Change dataTable layout (Table styling)
						// To use predefined layouts uncomment the line below and comment the custom lines below
						// doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return .5; };
						objLayout['vLineWidth'] = function(i) { return .5; };
						objLayout['hLineColor'] = function(i) { return '#aaa'; };
						objLayout['vLineColor'] = function(i) { return '#aaa'; };
						objLayout['paddingLeft'] = function(i) { return 4; };
						objLayout['paddingRight'] = function(i) { return 4; };
						doc.content[0].layout = objLayout;
				}
				},
            'print'
        ],
      ajax: '{{route("order.datatable")}}',
      "columns": [
        { "data": "id", "defaultContent": "" },
        { "data": "technosys_order_no", "defaultContent": "" },
        { "data": "orders[].product.name", "defaultContent": "" },
        { "data": "phone", "defaultContent": "" },
        { "data": "address", "defaultContent": "" },
        { "data": "total_amount", "defaultContent": "" },
        { "data": "created_at", "defaultContent": "" },
        { "data": "orderDelievered", "defaultContent": "" },
        { "data": "order_no", "defaultContent": "","className": "text-center" },
        { "data": "order_no", "defaultContent": "" },
       ],
      "columnDefs": [{
        "targets": 'no-sort',
        "orderable": false,
      },
      {
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + 1;
        },
      },
      {
        "targets": -4,
        "render": function (data, type, row, meta) {
          return moment(row.created_at).format('D MMM Y, H:m a');
        },
      },
      {
        "targets": -3,
        "render": function (data, type, row, meta) {
          // console.log();
          return row.order_delivered.delivery_status;
        },
      },
      {
        "targets": -2,
        "render": function (data, type, row, meta) {
          var url = "{{route('order.detail',[':order_no'])}}";
          url = url.replace(':order_no', data);
          return `<a href="`+url+`" style='font-size:25px;color: #dc3534;'><i class="fa fa-info-circle"></i></a>`;
        },
      },

      {
        "targets": -1,
        "render": function (data, type, row, meta) {
          return `
          <select class="p-2 order-status btn btn-primary" style='background-color:#dc3534 !important;border-color:white !important;' data-id="`+ data + `">
            <option value='0' ${row.status == 0 ? 'selected': ''}>In Process</option>
            <option value='1' ${row.status == 1 ? 'selected': ''}>On Way</option>
            <option value='2' ${row.status == 2 ? 'selected': ''}>Delievered</option>
          </select>
          `;
        },
      },
      ],
      "drawCallback": function (settings) {
        $('.order-status').change(function () {
          var $this = $(this);
          var id = $this.data('id');
          var status = $this.val();
          axios
            .post('{{route("order.status")}}', {
              _token: '{{csrf_token()}}',
              _method: 'post',
              orderNo: id,
              status: status,
            })
            .then(function (responsive) {
              console.log(responsive);
              swal(
                  'Success!',
                  'Status Updated Sucessfully.',
                  'success'
              )
            })
            .catch(function (error) {
              console.log(error);
            });
        });

      },
      //scrollX:true,
    });
    $('#advanceSearch').submit(function(e){
        e.preventDefault();
        var date = moment($('input[name="date"]').val(),'YYYY/MM/DD').format('YYYY-MM-DD');
        // if(date === 'Invalid date')
        // {
        //   date = '';
        // }
        // table.columns(1).search($('select[name="category_product"]').val());
        table.columns(1).search($('input[name="order_no"]').val());
        table.columns(5).search($('input[name="amount"]').val());
        // table.columns(5).search($('input[name="price"]').val());
        // table.columns(1).search($('input[name="name"]').val());
        table.columns(6).search(date);
        // table.columns(11).search($('.ver-status').val());
        table.draw();
      });

      $(".custom_datatable #DataTable_wrapper .row:nth-child(2) .col-sm-12").niceScroll();
  });

</script>
@endsection
