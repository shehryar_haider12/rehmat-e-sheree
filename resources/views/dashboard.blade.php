@extends('layouts.masterpage')

@section('title', 'Products')

@section('top-styles')
<!-- Plugins css-->
<link href="{{url('')}}/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

<!-- DataTables -->
<link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<style>
  .small-box .icon {
    -webkit-transition: all .3s linear;
    -o-transition: all .3s linear;
    transition: all .3s linear;
    position: absolute;
    top: -10px;
    right: 10px;
    z-index: 0;
    font-size: 90px;
    color: rgba(0,0,0,0.15);
  }
  .bg-yellow
  {
    background-color: #f39c12 !important;

  }
  .small-box {
    border-radius: 2px;
    position: relative;
    display: block;
    margin-bottom: 20px;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
  }
  .small-box>.inner {
      padding: 10px;
      color: white;
  }
</style>
@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                <h3 style="font-weight:bolder;">{{$total_categories}}</h3>
                <p style="font-size: 20px;">Total Categories</p>
                </div>
                <div class="icon">
                <i class="fa fa-sitemap"></i>
                </div>                
            </div>
          </div>
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                <h3 style="font-weight:bolder;">{{$total_products}}</h3>
                <p style="font-size: 20px;">Total Products</p>
                </div>
                <div class="icon">
                <i class="fa fa-product-hunt"></i>
                </div>                
            </div>
          </div>
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #dd4b39 !important">
                <div class="inner">
                <h3 style="font-weight:bolder;">{{$total_orders}}</h3>
                <p style="font-size: 20px;">Total Orders</p>
                </div>
                <div class="icon">
                <i class="fa fa-shopping-cart"></i>
                </div>                
            </div>
          </div>
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #dd4b39 !important">
                <div class="inner">
                <h3 style="font-weight:bolder;">{{$total_customers}}</h3>
                <p style="font-size: 20px;">Total Customers</p>
                </div>
                <div class="icon">
                <i class="fa fa-users"></i>
                </div>                
            </div>
          </div>
        </div>
    </div>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
  @parent
@endsection

@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/switchery/js/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
@endsection
