@extends('layouts.masterpage')

@section('title', 'Add Rider')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/plugins/select2/css/select2.min.css">
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }

</style>
@endsection

@section('header')
@parent
@endsection

@section('leftsidebar')
@parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-dark-theme waves-effect waves-light"
                        style="background-color: #dc3535 !important" type="button"
                        onclick="window.history.back(1)"><span class="btn-label"><i
                                class="fa fa-arrow-left"></i></span>Go back</button>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories')}}">Rider</a>
                    </li>
                    <li class="breadcrumb-item active">Add Rider</li>
                </ol>

            </div>
        </div>

        <form action="{{$isEdit ? route('riders.update',$rider->id) : route('riders.store')}}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @if ($isEdit)
            <input type="hidden" name="_method" value="put">
            @endif
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        <span class="ti-user mr-2"></span>Add Product</h3>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                        <button type="submit" class="btn btn-white waves-effect btn-rounded">
                            <span class="btn-label">
                                <i class="fa fa-save"></i>
                            </span> Save
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="name"
                                                parsley-trigger="change"  placeholder="name..."
                                                class="form-control" id="name" value="{{$rider->name ?? old('name') }}">
                                            <span class="text-danger">{{$errors->first('name')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contact
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="contact"
                                                parsley-trigger="change"  placeholder="contact..."
                                                class="form-control" id="contact" value="{{$rider->contact ?? old('contact') }}">
                                            <span class="text-danger">{{$errors->first('contact')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Duty Timings
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="duty_timings"
                                                parsley-trigger="change"  placeholder="duty_timings..."
                                                class="form-control" id="duty_timings" value="{{$rider->duty_timings ?? old('duty_timings') }}">
                                            <span class="text-danger">{{$errors->first('duty_timings')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Branch
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="branch"
                                                parsley-trigger="change"  placeholder="branch..."
                                                class="form-control" id="branch" value="{{$rider->branch ?? old('branch') }}">
                                            <span class="text-danger">{{$errors->first('branch')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Password
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="password"
                                                parsley-trigger="change"  placeholder="password..."
                                                class="form-control" id="password" value="">
                                            <span class="text-danger">{{$errors->first('password')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Confirm Password
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="password_confirmation"
                                                parsley-trigger="change"  placeholder="password_confirmation..."
                                                class="form-control" id="password_confirmation" value="">
                                            <span class="text-danger">{{$errors->first('password_confirmation')?? null}}</span>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    </form>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
@parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
    jQuery(document).ready(function () {

        $('form').parsley();
        $('.select2').select2();

        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });


    });

</script>
@endsection
