  @extends('layouts.masterpage')

@section('title', 'Rider Assign Order')

@section('top-styles')
<!-- Plugins css-->
<link href="{{url('')}}/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

<!-- DataTables -->
<link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

@endsection

@section('header')
  @parent
@endsection

@section('leftsidebar')
  @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <!-- <h4 class="page-title">Portlets</h4> -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item active">Rider Assign Order</li>
        </ol>
      </div>
    </div>

    <div class="portlet">
      <div class="portlet-heading bg-light-theme">
        <h3 class="portlet-title">
          <i class="ti-sharethis mr-2"></i> Rider Assign Order</h3>
        <div class="portlet-widgets">
           
        </div>
        <div class="clearfix"></div>
      </div>
      <div id="bg-primary1" class="panel-collapse collapse show">
        <div class="portlet-body">
          <div class="custom_datatable">

            <table id="datatable" class="table table-bordered table-striped table-responsive" width="100%" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th class="no-sort text-center" width="5%">S.No</th>
                  <th>Rider Name</th>
                  <th>Rider Phone</th>
                  <th>Order No</th>
                  <th>Collect</th>
                  <th>Collect At</th>
                  <th>Delivery Status</th>
                  <th>Delivered At</th>
                </tr>
              </thead>
            </table>

          </div>
        </div>
      </div>

    </div>
    <!-- end row -->


  </div>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
  @parent
@endsection

@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/switchery/js/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="{{url('')}}/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#datatable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{route("rider.assign-orders.datatable")}}',
      "columns": [
        { "data": "id", "defaultContent": "" },
        { "data": "rider_id", "defaultContent": "" },
        { "data": "rider_id", "defaultContent": "" },
        { "data": "technosys_order_no", "defaultContent": "" },
        { "data": "is_collect", "defaultContent": "" },
        { "data": "collect_at", "defaultContent": "null" },
        { "data": "delivery_status", "defaultContent": "null" },
        { "data": "delivered_at", "defaultContent": "null" },
       ],
      "columnDefs": [{
        "targets": 'no-sort',
        "orderable": false,
      },
      {
        "targets": 0,
        "render": function (data, type, row, meta) {
          return meta.row + 1;
        },
      },
      {
        "targets": 1,
        "render": function (data, type, row, meta) {
          return row.rider.name;
        },
      },
      {
        "targets": 2,
        "render": function (data, type, row, meta) {
          return row.rider.contact;
        },
      },
      {
        "targets": 4,
        "render": function (data, type, row, meta) {
          var val = data == 1 ? 'Yes' : 'No';
          return val;
        },
      },
      // {
      //   "targets": -1,
      //   "render": function (data, type, row, meta) {
      //     var checked = row.status == 1 ? 'checked' : null;
      //     return `
      //     <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` + checked + ` value="` + row.id + `">
      //     `;
      //   },
      // },
      ],
      "drawCallback": function (settings) {
        // var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
        // if (elems) {
        //   elems.forEach(function (html) {
        //     var switchery = new Switchery(html, {
        //       color: '#002644'
        //       , secondaryColor: '#dfdfdf'
        //       , jackColor: '#fff'
        //       , jackSecondaryColor: null
        //       , className: 'switchery'
        //       , disabled: false
        //       , disabledOpacity: 0.5
        //       , speed: '0.1s'
        //       , size: 'small'
        //     });

        //   });
        // }

        // $('.status').change(function () {
        //   var $this = $(this);
        //   var id = $this.val();
        //   var status = this.checked;

        //   if (status) {
        //     status = 1;
        //   } else {
        //     status = 0;
        //   }

        //   axios
        //     .post('{{route("rider.status")}}', {
        //       _token: '{{csrf_token()}}',
        //       _method: 'patch',
        //       id: id,
        //       status: status,
        //     })
        //     .then(function (responsive) {
        //       console.log(responsive);
        //     })
        //     .catch(function (error) {
        //       console.log(error);
        //     });
        // });
      },
      //scrollX:true,
    });
  });

</script>
@endsection
