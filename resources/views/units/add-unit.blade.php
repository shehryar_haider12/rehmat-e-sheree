@extends('layouts.masterpage')

@section('title', 'Add Unit')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/plugins/select2/css/select2.min.css">
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }

</style>
@endsection

@section('header')
@parent
@endsection

@section('leftsidebar')
@parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-dark-theme waves-effect waves-light"
                        style="background-color: #dc3535 !important" type="button"
                        onclick="window.history.back(1)"><span class="btn-label"><i
                                class="fa fa-arrow-left"></i></span>Go back</button>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories')}}">Unit</a>
                    </li>
                    <li class="breadcrumb-item active">Add Unit</li>
                </ol>

            </div>
        </div>

        <form action="{{$isEdit ? route('unit.update',$unit->id) : route('unit.store')}}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @if ($isEdit)
            <input type="hidden" name="_method" value="put">
            @endif
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        <span class="ti-user mr-2"></span>Add Unit</h3>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                        <button type="submit" class="btn btn-white waves-effect btn-rounded">
                            <span class="btn-label">
                                <i class="fa fa-save"></i>
                            </span> Save
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Unit
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="name" parsley-trigger="change" required placeholder="Name..." class="form-control" id="name" value="{{$unit->name ?? old('name') }}">
                                            <span class="text-danger">{{$errors->first('name')?? null}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    </form>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
@parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
    jQuery(document).ready(function () {

        $('form').parsley();
        $('.select2').select2();
    });

</script>
@endsection
