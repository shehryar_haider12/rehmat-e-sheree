<!-- Left Sidebar Start -->
{{-- @php
  $menus = Auth::user()->user_type == 0 || Auth::user()->user_type == 2 ? getMenus() : getUserRoleMenus(Auth::user()->role_id);
@endphp --}}
<div class="left side-menu" style="background-color: black;" >
  <div class="sidebar-inner slimscrollleft">
    <!--- Divider -->
    <div id="sidebar-menu">
      <ul style="margin-top: 15px;">
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('dashboard')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-dashboard"></i>
            <span> Dashboard</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('customers')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-users"></i>
            <span> Customers</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('riders')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-users"></i>
            <span> Riders</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('categories')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-list"></i>
            <span> Categories</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('units')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-balance-scale"></i>
            <span>Units</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('products')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-product-hunt"></i>
            <span> Products</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('promotions')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-television"></i>
            <span> Promotions</span>
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('sliders')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-picture-o"></i>
            <span> Sliders</span>
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('discounts')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-percent"></i>
            <span> Discounts</span>
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('notification')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-bell"></i>
            <span> Notification</span>
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('orders')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-shopping-cart"></i>
            <span> Orders</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('rider.assign-orders')}}" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-shopping-cart"></i>
            <span> Rider Assign Order</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="javascript:void(0);" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-usd"></i>
            <span class='menu-arrow'></span>Transaction
          </a>
            <ul class="list-unstyled">
              <li>
                <a href="{{route("transaction")}}" style="padding: 10px 0px 10px 23px;">
                  <i class="fa fa-usd"></i>
                  Transaction
                </a>
              </li>
            </ul>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="javascript:void(0);" class="waves-effect" style="border-color: #064474">
            <i class="fa fa-file"></i>
            <span class='menu-arrow'></span>Reports
          </a>
            <ul class="list-unstyled">
              <li>
                <a href="{{route("reports")}}" style="padding: 10px 0px 10px 23px;">
                  <i class="fa fa-file"></i>
                  Reports
                </a>
              </li>
            </ul>
        </li>
        <li class="has_sub" style="background: #002644 !important">
          <a href="{{route('site.settings')}}" class="waves-effect" style="border-color: #064474">
            <i class="md md-settings"></i>
            <span> Settings</span>
            {{-- <span class='menu-arrow'></span> --}}
          </a>
        </li>
      </ul>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- Left Sidebar End -->
