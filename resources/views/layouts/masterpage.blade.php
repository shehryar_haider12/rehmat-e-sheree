<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Rehmat-e-Sheeren - @yield('title')</title>

  @yield('top-styles')

  <link href="{{ url('') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{ url('') }}/assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{ url('') }}/assets/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{ url('') }}/assets/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{ url('') }}/assets/js/modernizr.min.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.0.2/firebase-app.js"></script>
  <link rel="manifest" href="{{url('')}}/manifest.json">
  <link rel="manifest" href="{{url('')}}/firebase-messaging-sw.js">
  <script src="https://www.gstatic.com/firebasejs/8.0.2/firebase-messaging.js"></script>
  <style>
    .bg-light-theme
    {
      background-color: #123957 !important;
    }
    .custom_datatable table tr th
    {
      background: #005ca3 !important;
    }
    .custom_datatable .pagination li.paginate_button.active a
    {
      background: #005ca3 !important;
      border-color: white;
    }
    .breadcrumb a
    {
      color: #005ca3;
    }
    .side-menu.left #sidebar-menu ul>li>a.active, .side-menu.left #sidebar-menu ul>li>a:hover
    {
      color: #005ca3 !important;

    }
    .side-menu.left #sidebar-menu .subdrop
    {
      color: #005ca3 !important;
    }
    span.badge.badge-warning.navbar-badge.number-alert {
        position: relative;
        bottom: 15px;
        right: 10px;
        background-color: #005ca3;
        color: #ffffffe0;

    }
    .list-inline-item:not(:last-child) {
      border-right: 1px solid gray;
    }
  </style>

</head>


<body class="fixed-left">

  <!-- Begin page -->
  <div id="wrapper">

    @section('header')
      @include('layouts.header')
    @show

    @section('leftsidebar')
      @include('layouts.leftsidebar')
    @show



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">

      @yield('content')


      <footer class="footer text-right">
        <a href="" target="_blank"></a>. All rights reserved.
      </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

  </div>
  <!-- END wrapper -->



  <script>
    var resizefunc = [];
  </script>
  <!-- axios -->
  <script src="{{url('')}}/assets/js/sweetalert2.min.js"></script>
  <script src="{{url('')}}/assets/js/axios.min.js"></script>
  <!-- jQuery  -->
  <script src="{{ url('') }}/assets/js/jquery.min.js"></script>
  <script src="{{ url('') }}/assets/js/popper.min.js"></script>
  <!-- Popper for Bootstrap -->
  <script src="{{ url('') }}/assets/js/bootstrap.min.js"></script>
  <script src="{{ url('') }}/assets/js/detect.js"></script>
  <script src="{{ url('') }}/assets/js/fastclick.js"></script>
  <script src="{{ url('') }}/assets/js/jquery.slimscroll.js"></script>
  <script src="{{ url('') }}/assets/js/jquery.blockUI.js"></script>
  <script src="{{ url('') }}/assets/js/waves.js"></script>
  <script src="{{ url('') }}/assets/js/wow.min.js"></script>
  <script src="{{ url('') }}/assets/js/jquery.nicescroll.js"></script>
  <script src="{{ url('') }}/assets/js/jquery.scrollTo.min.js"></script> @yield('bottom-mid-scripts')
  <script src="{{url('')}}/plugins/moment/moment.js"></script>
  
  <script src="{{ url('') }}/assets/js/jquery.core.js"></script>
  <script src="{{ url('') }}/assets/js/jquery.app.js"></script> @yield('bottom-bot-scripts')
  <script src="{{url('')}}/js/firbase.js"></script>
<script>
  $('#read-notification').click(function () {
      var $this = $(this);
      var id = $this.data('user');
      // console.log('adfadfsf');
      axios
          .post('{{route("notification.read")}}', {
          _token: '{{csrf_token()}}',
          _method: 'patch',
          id: id,
          })
          .then(function (responsive) {
              $('.number-alert').empty().html(0);
              // $('.data-notify').empty();
              console.log(responsive);
          })
          .catch(function (error) {
          console.log(error);
          });
  });
</script>
</body>

</html>
