<!-- Top Bar Start -->
<div class="topbar" style="">

  <!-- LOGO -->
  <div class="topbar-left" style="background-color: #1a2642 !important">
    <div class="text-center">
      <a href="{{url('/dashboard')}}" class="logo">
        <span class="fs40" style="color: white">
            RS
        </span>
      </a>  
    </div>
  </div>

  <!-- Button mobile view to collapse sidebar menu -->
  <nav class="navbar-custom" style="background-color: #002644 !important">

    <ul class="list-inline float-right mb-0">
      <li class="list-inline-item dropdown notification-list">
        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-user="{{Auth::user()->id}}" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false" id="read-notification">
          <i class="fa fa-bell" style="margin-right: 8px; font-size: 25px !important;color:white;"></i>
          <span class="badge badge-warning navbar-badge number-alert">0</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview" style="padding-bottom: 0px">
          <div class="dropdown-item noti-title" style="border-bottom: 1px solid #80808082; padding-top: 0px !important;">
            <h5 class="text-overflow" style="text-align: center">
              <small>0 Unread Notification..</small>
            </h5>
          </div>
 
          <a href="{{route('users.settings')}}" class="dropdown-item notify-item" style="border-bottom: 1px solid #80808082;">
            <i class="fa fa-shopping-cart"></i>
            <span>new order from anus ahmed karachi... </span>
          </a>
          <a href="{{route('users.settings')}}" class="dropdown-item notify-item" style="border-bottom: 1px solid #80808082;">
            <i class="fa fa-shopping-cart"></i>
            <span>new order from anus ahmed karachi... </span>
          </a>
          <a href="{{route('users.settings')}}" class="dropdown-item notify-item" style="border-bottom: 1px solid #80808082;">
            <i class="fa fa-shopping-cart"></i>
            <span>new order from anus ahmed karachi... </span>
          </a>
          <div class="dropdown-item noti-title" style="background-color:#80808014;">
            <h5 class="text-overflow" style="text-align: center">
              <small>See all notification</small>
            </h5>
          </div>
        </div>
      </li>
      <li class="list-inline-item dropdown notification-list">
        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
          @if (Auth::user()->avatar)
          <img src="{{url('').'/uploads/'.Auth::user()->avatar}}" alt="user" class="rounded-circle">
          @else
              @php
                  $name = explode(" ",Auth::user()->name);
                  $count = 1;
              @endphp
            <div style="background-color: #ebeff2; border-radius: 50px; width: 50px; height: 50px; text-align: center; padding-bottom: 0px !important; line-height: 52px; font-weight:900; color:#e74c3c;">
              @foreach ($name as $item){{strtoupper($item[0] ?? null)}}
                @php 
                  $count++; 
                  if($count>2) break; 
                @endphp
              @endforeach
            </div>
          @endif
        </a>
        <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
          <div class="dropdown-item noti-title">
            <h5 class="text-overflow">
              <small>Welcome {{Auth::user()->name}} !</small>
            </h5>
          </div>

          <a href="{{route('users.settings')}}" class="dropdown-item notify-item">
            <i class="md md-settings"></i>
            <span>Profile</span>
          </a>

          <a href="{{route('logout')}}" class="dropdown-item notify-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i>
            <span>Logout</span>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
      <li class="list-inline-item notification-list">
        <a class="nav-link waves-light waves-effect" href="#" id="btn-fullscreen">
          <i class="dripicons-expand noti-icon"></i>
        </a>
      </li>
      
    </ul>

    <ul class="list-inline menu-left mb-0">
      <li class="float-left">
        <button class="button-menu-mobile open-left waves-light waves-effect bg-dark-theme" style="background-color: black">
          <i class="dripicons-menu"></i>
        </button>
      </li>
    </ul>

  </nav>

</div>
<!-- Top Bar End -->
