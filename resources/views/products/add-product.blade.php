@extends('layouts.masterpage')

@section('title', 'Add Product')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/plugins/select2/css/select2.min.css">
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }

</style>
@endsection

@section('header')
@parent
@endsection

@section('leftsidebar')
@parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-dark-theme waves-effect waves-light"
                        style="background-color: #dc3535 !important" type="button"
                        onclick="window.history.back(1)"><span class="btn-label"><i
                                class="fa fa-arrow-left"></i></span>Go back</button>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories')}}">Product</a>
                    </li>
                    <li class="breadcrumb-item active">Add Product</li>
                </ol>

            </div>
        </div>

        <form action="{{$isEdit ? route('product.update',$product->id) : route('product.store')}}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @if ($isEdit)
            <input type="hidden" name="_method" value="put">
            @endif
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        <span class="ti-user mr-2"></span>Add Product</h3>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                        <button type="submit" class="btn btn-white waves-effect btn-rounded">
                            <span class="btn-label">
                                <i class="fa fa-save"></i>
                            </span> Save
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="avatar-upload" style="height: 270px">
                                            <div class="avatar-edit">
                                                <input type='file' name="featured_image" id="imageUpload1"
                                                    accept=".png, .jpg, .jpeg" />
                                                <label for="imageUpload1"><span>Featured Image </span></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="imagePreview1"
                                                    style="background-image : url({{url('').'/uploads/'}}{{$product->featured_image ?? 'placeholder.jpg'}}">
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-danger">{{$errors->first('featured_image') ?? null}}</span>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="form-group col-md-2">
                                                <div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="changePasswordCheckbox" name="is_featured" @if($isEdit){{ $product->is_featured == 1 ? 'checked' : null }}@endif value="1">
                                                        <label class="custom-control-label" for="changePasswordCheckbox">Is Featured</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customizeCheckbox" name="customize_box" @if($isEdit){{ $product->customize_box == 1 ? 'checked' : null }}@endif value="1">
                                                        <label class="custom-control-label" for="customizeCheckbox">Show on Customize Box</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-10">
                                                <label for="">Category<span class="text-danger">*</span>
                                                </label>
                                                <select parsley-trigger="change" data-style="btn-white" name="category_id[]"
                                                    class="form-control select2" multiple="multiple" >
                                                    @if ($isEdit)
                                                    @foreach ($categories as $category)
                                                    @foreach ($product->categoryProduct as $item)
                                                    <option value="{{$category->id}}" {{ $item->category_id == $category->id ? 'selected' : null }}>
                                                        {{$category->name}}</option>
                                                    @endforeach
                                                    @endforeach
                                                    @else
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}"> {{$category->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Code
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="code" parsley-trigger="change"  placeholder="Code..." class="form-control" id="code" value="{{$product->code ?? old('code') }}">
                                            <span class="text-danger">{{$errors->first('code')?? null}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="name" parsley-trigger="change" 
                                                placeholder="Name..." class="form-control" id="name"
                                                value="{{$product->name ?? old('name') }}">
                                            <span class="text-danger">{{$errors->first('name')?? null}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Price
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="number" name="price" data-parsley-type="digits"
                                                parsley-trigger="change"  placeholder="Price..."
                                                class="form-control" id="price" value="{{$product->price ?? old('price') }}">
                                            <span class="text-danger">{{$errors->first('price')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Weight
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" name="weight" parsley-trigger="change" 
                                                placeholder="weight..." class="form-control" id="weight"
                                                value="{{$product->weight ?? old('weight') }}" data-parsley-type="number" data-parsley-maxlength="3">
                                            <span class="text-danger">{{$errors->first('weight')?? null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Units<span class="text-danger">*</span>
                                        </label>
                                        <select parsley-trigger="change" data-style="btn-white" name="unit_id" class="form-control select2" >
                                            <option value="" disabled selected>Select Unit</option>
                                            @foreach ($units as $unit)
                                                <option value="{{$unit->id}}" @if ($isEdit) {{$product->unit_id == $unit->id ? 'selected' : null}} @endif> {{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Product Sub Images
                                                <span class="text-danger">*</span>
                                            </label>
                                        <input type="file" multiple name="images[]" parsley-trigger="change" class="form-control" id="images">
                                            <span class="text-danger">{{$errors->first('images')?? null}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description
                                                <span class="text-danger">*</span>
                                            </label>
                                            <textarea name="description" class="form-control" id="description"
                                                cols="180" rows="4" placeholder="Description" >
                                          {{$product->description ?? null }}
                                        </textarea>
                                            <span class="text-danger">{{$errors->first('description')?? old('description')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="section row">
                                    @if ($isEdit)
                                        @foreach ($product->productImages as $item)
                                            <div class="images col-md-2">
                                                <a href="{{route('product_image.delete',$item->id)}}"><span class="fa fa-close"></span></a>
                                                <img src="{{url('').'/uploads/'.$item->images}}" height='121px' width='190px'>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    </form>

</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
@parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
    jQuery(document).ready(function () {

        $('form').parsley();
        $('.select2').select2();

        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });


    });

</script>
@endsection
