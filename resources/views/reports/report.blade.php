@extends('layouts.masterpage')

@section('title', 'Report')

@section('top-styles')
<link href="{{url('')}}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> @endsection
<style>
  .theme-input-group .input-group-prepend .input-group-text
  {
    background-color: #dc3535 !important;
  }
</style>
@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li class="breadcrumb-item">
            <a href="#">Reports</a>
          </li>
          <li class="breadcrumb-item active">Report</li>
        </ol>

      </div>
    </div>
    <form action="{{route('search.reports')}}" method="post">
      @csrf
      <div class="portlet">
        <div class="portlet-heading bg-light-theme">
          <h3 class="portlet-title">
            <span class="ti-bar-chart mr-2"></span>Report</h3>
          <div class="portlet-widgets">
            <br />
          </div>
          <div class="clearfix"></div>
        </div>

        <div id="bg-inverse" class="panel-collapse collapse show" style="">
          <div class="portlet-body">

            <div class="card-box m-b-0">

              <div class="row">
                <div class="col-md-8 mx-auto">

                  <div class="form-group">
                    <div class="input-group theme-input-group select-max-h-200">
                      <div class="input-group-prepend width-160px">
                        <div class="input-group-text">Nature of Reports</div>
                      </div>
                      {{--
                        <select data-style="btn-white" parsley-trigger="change" required name="nature" class="  form-control"
                        tabindex="-98"> --}}
                        <select data-style="btn-white" parsley-trigger="change" required name="nature" class="form-control" data-parsley-errors-container="#NatureOfReportErrorContainer" >
                          <option selected="" disabled="" value="Null">Please pick a Nature</option>
                          <option value="1">Sale Wise Report</option>
                          {{-- <option value="3">Report2</option>
                          <option value="4">Report3</option>
                          <option value="5">Report4</option> --}}
                        </select>
                      </div>
                    </div>
                    <span class="text-danger" id="NatureOfReportErrorContainer"></span>

                  <div class="form-group">
                    <div class="input-group theme-input-group select-max-h-200">
                      <div class="input-group-prepend width-160px">
                        <div class="input-group-text">Start Date</div>
                      </div>
                      <input parsley-trigger="change" required type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" placeholder="Pick Start Date">
                    </div>
                    <!-- input-group -->
                    <div class="input-group theme-input-group select-max-h-200">
                      <div class="input-group-prepend width-160px">
                        <div class="input-group-text">End Date</div>
                      </div>
                      <input parsley-trigger="change" required type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" placeholder="Pick End Date">
                    </div>
                
                    <!-- input-group -->
                  </div>

                  <div class="text-right">
                    <button class="btn btn-light-theme waves-effect waves-light" style="background-color: #dc3535 !important" type="submit">
                      <i class="fa fa-search pr-1"></i> Search</button>
                  </div>

                </div>
              </div>

            </div>

          </div>
        </div>
      </div>

    </form>


  </div>
  <!-- container -->
</div>
<!-- content -->
@endsection
@section('bottom-mid-scripts')

<script src="{{url('')}}/plugins/moment/moment.js"></script>
<script src="{{url('')}}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
  jQuery(document).ready(function () {
    // Date Picker
    $('#start_date').datepicker({
      autoclose: true,
      todayHighlight: true,
      startDate: '2000-01-01',
      endDate: new Date(new Date().setDate(new Date().getDate() + 1)),
      format: 'yyyy-mm-dd',
    });
    $('#start_date').change(function(){
      var date = new Date($('#start_date').val());
      var date1 = date.getDate();      
      var month1 = date.getMonth() + 1;      
      var year1 = date.getFullYear();      

      $('#end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: year1+'-'+month1+'-'+date1,
        endDate: new Date(new Date().setDate(new Date().getDate())),
        format: 'yyyy-mm-dd',
      });
    });   
    $('form').parsley();
  });
</script>
@endsection
