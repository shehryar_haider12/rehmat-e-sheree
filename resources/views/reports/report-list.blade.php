
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
    <link href="{{ url('') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/css/style.css" rel="stylesheet" type="text/css" />

    <link href="{{ url('') }}/assets/css/custom.css" rel="stylesheet" type="text/css" />
    
    {{-- <link href="{{ url('') }}/css/app.css" rel="stylesheet" type="text/css" /> --}}
  <!-- DataTables -->
  <link href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
  <!-- Responsive datatable examples -->
  <link href="{{url('')}}/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

  <style>
    .custom_datatable table tfoot tr td {
          background-color: rgb(220 53 53) !important;
      }
    .bg-light-theme
    {
      background-color: #dc3535 !important;
    }
    .custom_datatable table tr th
    {
      background: #dc3535 !important;
    }
    .custom_datatable .pagination li.paginate_button.active a
    {
      background: #dc3535 !important;
      border-color: white;
    }
    table.table tbody tr td {
        word-wrap: break-word;
        word-break: break-all;
    }
  </style>

</head>
<body style="font-size: 11px;height:100%;">

  <!-- ============================================================== -->
  <!-- Start right Content here -->
  <!-- ============================================================== -->
  <!-- Start content -->
  <div class="content">
      <div class="container-fluid">

          <div class="portlet">
              <div class="portlet-heading bg-light-theme">
                  <h3 class="portlet-title">
                      <i class="ti-menu-alt mr-2"></i> Reports List</h3>
                  <div class="portlet-widgets">
                      <span class="divider"></span>
                      <span class="divider"></span>
                  </div>
                  <div class="clearfix"></div>
              </div>
              <div id="bg-primary1" class="panel-collapse collapse show">
                  <div class="portlet-body">

                      <div class="row align-items-center">
                          <div class="col-md-3">
                              {{-- <img src="{{url('')}}/uploads/placeholder.jpg" width="100" alt="" /> --}}
                          </div>

                          <div class="col-md-6 text-center">
                              <h4 class="m-0">
                                  <b>REPORT SHEET</b>
                              </h4>
                              <h4 class="m-0">
                                  <b>{{$nature}}</b>
                              </h4>
                              <h6 class="m-0">
                                  <b>REPORT DURATION
                                      <span class="text-light-theme" style="color: #dc3535">FROM</span> :
                                      {{$start_date}},
                                      <span class="text-light-theme" style="color: #dc3535">TO</span> :
                                      {{$end_date}}</b>
                              </h6>
                              <h6 class="m-0">
                                  <b>REPORT DATE : {{date('yy-m-d')}}</b>
                              </h6>
                          </div>

                          <div class="col-md-3 text-right">
                              {{-- <img src="{{url('')}}/uploads/placeholder.jpg" width="100" alt="" /> --}}
                          </div>
                      </div>
                    <hr>
                      {{-- <div class="custom_datatable"> --}}
                          <table class="table table-bordered table-striped" width="100%">
                              <thead style="background-color: #dc3535; color:white;">
                                  <tr>
                                      <th>Order No</th>
                                      <th>Client Name</th>
                                      <th>Item Name</th>
                                      <th>Phone</th>
                                      <th>Address</th>
                                      <th>Date</th>
                                      <th>Price</th>
                                      <th>Quantity</th>
                                      <th>Amount</th>
                                  </tr>
                              </thead>
                            @php
                                $i=1;
                            @endphp
                              <tbody>
                                @foreach ($order_data as $orders)
                                @php
                                    $i++;
                                @endphp
                                  @foreach ($orders->orders as $order)
                                  <tr>
                                      <td>{{$orders->order_no}}</td>
                                      <td>{{$orders->user->name}}</td>
                                      <td>{{$order->product->name}}</td>
                                      <td>{{$orders->phone}}</td>
                                      <td>{{$orders->address}}</td>
                                      <td>{{$orders->date}}</td>
                                      <td>{{$order->product->price}}</td>
                                      <td>{{$order->quantity}}</td>
                                      <td>{{$order->amount}}</td>
                                  </tr>
                                  @endforeach
                                  @if ($orders->order_no == $order->order_no)
                                  <tr>
                                      <td colspan="5"></td>
                                      <td colspan="3"
                                          style="background: #002644d4; color: white; font-weight: bold; font-size: 10px;">
                                          Delivery Charges</td>
                                      <td colspan="1"
                                          style="background: #002644d4; color: white; font-weight: bold; font-size: 10px;">
                                          {{$orders->delievery_charges}}</td>
                                  </tr>
                                  <tr>
                                      <td colspan="5"></td>
                                      <td colspan="3"
                                          style="background: #002644d4; color: white; font-weight: bold; font-size: 10px;">
                                          Total Amount</td>
                                      <td colspan="1"
                                          style="background: #002644d4; color: white; font-weight: bold; font-size: 10px;">
                                          {{$orders->total_amount}}</td>
                                  </tr>
                                  @endif
                                  @endforeach      
                              </tbody>
                              <tfoot style="background-color: #dc3535; color:white;">
                                  <tr>
                                      <td colspan="5"></td>
                                      <td colspan="3"
                                          style="border-color:none !important; font-weight: bold; font-size: 17px;">
                                          Grand Total Amount</td>
                                      <td colspan="1"
                                          style="border-color:none !important; font-weight: bold; font-size: 17px;">
                                          {{$grand_total}}</td>
                                  </tr>
                              </tfoot>
                          </table>
                      {{-- </div> --}}
                      <div class="clearfix"></div>
                      <!-- custom_datatable -->

                  </div>
                  <!--portlet-body-->

              </div>

          </div>
          <!-- portlet -->

      </div>
      <!-- container -->
  </div>
  <!-- content -->
</body>
</html>