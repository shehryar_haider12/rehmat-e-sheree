@extends('layouts.masterpage')

@section('title', 'Site Settings')

@section('top-styles')
<link href="{{url('')}}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('header')
    @parent
@endsection

@section('leftsidebar')
    @parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">
        <div class="btn-group pull-right">
          <button class="btn btn-dark-theme waves-effect waves-light" style="background-color: #dc3535 !important" type="button" onclick="window.history.back(1)"><span
              class="btn-label"><i class="fa fa-arrow-left"></i></span>Go back</button>
        </div>

        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
          <li class="breadcrumb-item active">Site Settings</li>
        </ol>

      </div>
    </div>

    <form action="{{route('site.settings.store')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="portlet">
        <div class="portlet-heading bg-light-theme">
          <h3 class="portlet-title"><span class="ti-user mr-2"></span>Site Settings</h3>
          <div class="portlet-widgets">
            <span class="divider"></span>
            <button type="submit" class="btn btn-white waves-effect btn-rounded">
              <span class="btn-label"><i class="fa fa-save"></i></span> Save
            </button>
          </div>
          <div class="clearfix"></div>
        </div>

        <div id="bg-inverse" class="panel-collapse collapse show" style="">
          <div class="portlet-body">

            <div class="card-box">
              <div class="row">
              <div class="row col-md-12">
                  <div class="form-group col-md-4">
                    <label>Delievery Charges <span class="text-danger">*</span></label>
                    <input type="number" name="delievery_charges" parsley-trigger="keyup" required placeholder="Amount..."  class="form-control" id="userName" value="{{$setting->where('key','delievery_charges')->first()->value ?? old('delievery_charges')}}" data-parsley-type="number">
                  </div>
                  <div class="form-group col-md-2">
                    <div class="mt30">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="changePasswordCheckbox" name="checkbox" {{ isset($setting->where('key','free_delievery_charges_after')->first()->value) ? 'checked' : null }} value="1">
                        <label class="custom-control-label" for="changePasswordCheckbox">Click for add</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-4">
                    <label>Free Delievery Charges After <span class="text-danger">*</span></label>
                    <input type="number" name="free_delievery_charges_after" parsley-trigger="keyup" required="" placeholder="Enter Amount..." class="form-control" value="{{$setting->where('key','free_delievery_charges_after')->first()->value ?? old('free_delievery_charges_after')}}" data-parsley-type="number">
                  </div>
              </div>
              </div>
            </div>

          </div>
        </div>

      </div>

    </form>

  </div><!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
    @parent
@endsection

@section('bottom-mid-scripts')
<script src="{{url('')}}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
  jQuery(document).ready(function() {

  function readURL(input, number) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
          $('#imagePreview' + number).hide();
          $('#imagePreview' + number).fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imageUpload1").change(function () {
    readURL(this, 1);
  });
        // Date Picker
    // jQuery('#dob').datepicker({
    //     format: 'yyyy-mm-dd',
    // });

    $('form').parsley();

    $('#changePasswordCheckbox').click(function(){
      if($(this).prop('checked'))
      {
        $('input[name="free_delievery_charges_after"]').removeAttr('disabled');
        $('input[name="free_delievery_charges_after"]').attr('required',true);
      }
      else
      {
        $('input[name="free_delievery_charges_after"]').removeAttr('required');
        $('input[name="free_delievery_charges_after"]').attr('disabled',true);
      }
    });

    if($('#changePasswordCheckbox').prop('checked'))
    {
      $('input[name="free_delievery_charges_after"]').removeAttr('disabled');
      $('input[name="free_delievery_charges_after"]').attr('required',true);
    }
    else
    {
      $('input[name="free_delievery_charges_after"]').removeAttr('required');
      $('input[name="free_delievery_charges_after"]').attr('disabled',true);
    }
  });
</script>
@endsection
