<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A Content Management System designed by horizontech.biz">
    <meta name="author" content="horizontech.biz">

    <link rel="shortcut icon" href="{{url('')}}/assets/images/favicon3.ico">

    <title>Confirm Payment</title>

    <link href="{{url('')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('')}}/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="{{url('')}}/assets/css/style.css" rel="stylesheet" type="text/css" />

    <link href="{{url('')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('')}}/assets/css/custom.css" rel="stylesheet" type="text/css" />

    <script src="{{url('')}}/assets/js/modernizr.min.js"></script>
    <style>
        .bg-dark-theme {
            background: #92000e !important;

        }

    </style>
</head>

<body>
    {{-- {{dd($data['full_name'])}} --}}
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        {{-- <span class="ti-user mr-2"></span>Confirm Payment</h3> --}}
                        <div class="portlet-widgets">
                            <span class="divider"></span>
                        </div>
                        <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row ">


                                    <form role="form" id="payment_form" class="col-md-11" action="{{route('confirm')}}"
                                        method="post" style="margin:0 auto ">
                                        @csrf
                                        <div class="head">
                                            <h2 class="text-center">Confirm Payment</h2>
                                            <hr>
                                        </div>
                                        <input type="hidden" name="access_key" value="87b65912a93a338888aa9072c3509d7e">
                                        <input type="hidden" name="profile_id"
                                            value="9399E91E-7D2C-44FA-83F0-C325D4800C60">
                                        <input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">


                                        <input type="hidden" name="signed_field_names"
                                            value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,bill_to_address_country,bill_to_address_state,bill_to_address_postal_code,bill_to_address_city,bill_to_address_line1,bill_to_forename,bill_to_surname,bill_to_email,consumer_id,merchant_defined_data1,merchant_defined_data2,merchant_defined_data4,merchant_defined_data5,merchant_defined_data7,merchant_defined_data20,customer_ip_address,bill_to_phone,amount,currency">
                                        <input type="hidden" name="unsigned_field_names">
                                        <input type="hidden" name="signed_date_time"
                                            value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                                        <input type="hidden" name="locale" value="en">
                                        <input type="hidden" name="transaction_type" size="25" value="sale">
                                        <input type="hidden" name="reference_number" size="25"
                                            value="<?php echo uniqid() ?>">
                                        <div class="row">
                                            @php
                                            $name = explode(' ',$data['full_name']);
                                            @endphp
                                            <div class="form-group col-md-3">
                                                <label for="InputName">First Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_forename"
                                                        id="fname" placeholder="First Name" required=""
                                                        value="{{$name[0]}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Last Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_surname"
                                                        id="lname" placeholder="Last Name" required=""
                                                        value="{{$name[1]}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputEmail">Enter Email</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control" id="InputEmailFirst"
                                                        name="bill_to_email" placeholder="Enter Email" required=""
                                                        value="{{$data['bill_to_email']}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputName">Phone</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_phone"
                                                        id="phone" placeholder="Phone" required=""
                                                        value="{{$data['bill_to_phone']}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="InputName">Address</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_address_line1"
                                                        id="address" placeholder="Address" required=""
                                                        value="{{$data['bill_to_address_line1']}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputName">City</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_address_city"
                                                        id="city" placeholder="City" required=""
                                                        value="{{$data['bill_to_address_city']}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-3">
                                                <label for="InputName">Country</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" value="PK"
                                                        name="bill_to_address_country" id="country" required="">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <div>
                                                    <label for="InputName">Postal Code</label>
                                                </div>
                                                <div>
                                                    <input type="text" class="form-control"
                                                        name="bill_to_address_postal_code" id="zip"
                                                        placeholder="Postal code"
                                                        value="{{$data['bill_to_address_postal_code']}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <div>
                                                    <label for="InputName">State</label>
                                                </div>
                                                <div>
                                                    <input type="text" class="form-control" value="SN"
                                                        name="bill_to_address_state" id="country" required="">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Amount</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="amount" id="amount"
                                                        placeholder="Amount" required="" value="{{$data['amount']}}">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Currency</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="currency" id="currency"
                                                        placeholder="Currency" value="">
                                                        <option value="pkr" selected>PKR</option>
                                                    </select>
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="consumer_id" name="consumer_id"
                                            value="<?php echo uniqid(); ?>">

                                        <input type="hidden" id="channel of operations" name="merchant_defined_data1"
                                            value="WC">
                                        <input type="hidden" name="merchant_defined_data2" value="YES">
                                        <input type="hidden" name="merchant_defined_data4" value="Voucher Book">
                                        <input type="hidden" name="merchant_defined_data5" value="Standard">
                                        <input type="hidden" name="merchant_defined_data7" value="NO">
                                        <input type="hidden" name="merchant_defined_data20" value="NO">
                                        <input type="hidden" id="customer_ip_address" name="customer_ip_address"
                                            size="25" value="127.0.0.1">
                                        <input type="submit" name="submit" id="submit" value="Confirm"
                                            class="btn btn-info pull-right"><br><br><br>
                                        <input type="text" id="decision" name="returns_accepted" value=""
                                            style="visibility:hidden;">
                                        {{-- <input type="hidden" id="signature" name="signature" value="A2w5KUZ600ROM5cuQVmsC0scIV1NYPXOk2lS5EG4bjQ="> --}}
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <!-- container -->
    </div>
    <!-- content -->
    <!-- jQuery  -->
    <script src="{{url('')}}/assets/js/jquery.min.js"></script>
    <script src="{{url('')}}/assets/js/popper.min.js"></script>
    <!-- Popper for Bootstrap -->
    <script src="{{url('')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{url('')}}/assets/js/detect.js"></script>
    <script src="{{url('')}}/assets/js/fastclick.js"></script>
    <script src="{{url('')}}/assets/js/jquery.slimscroll.js"></script>
    <script src="{{url('')}}/assets/js/jquery.blockUI.js"></script>
    <script src="{{url('')}}/assets/js/waves.js"></script>
    <script src="{{url('')}}/assets/js/wow.min.js"></script>
    <script src="{{url('')}}/assets/js/jquery.nicescroll.js"></script>
    <script src="{{url('')}}/assets/js/jquery.scrollTo.min.js"></script>

    <!-- lOGIN Page Plugins -->
    <script src="{{url('')}}/assets/pages/login/EasePack.min.js"></script>
    <script src="{{url('')}}/assets/pages/login/rAF.js"></script>
    <script src="{{url('')}}/assets/pages/login/TweenLite.min.js"></script>
    <script src="{{url('')}}/assets/pages/login/login.js"></script>

    <script src="{{url('')}}/assets/js/jquery.core.js"></script>
    <script src="{{url('')}}/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {

            // Init CanvasBG and pass target starting location
            CanvasBG.init({
                Loc: {
                    x: window.innerWidth / 2,
                    y: window.innerHeight / 3.3
                },
            });

            fetch('https://ipapi.co/json/')
                .then(response => response.json())
                .then((response) => {
                    $('#loginForm').append(`<input type="hidden" name="last_login_details" value='` + JSON
                        .stringify(response) + `'>`);
                })
        });

    </script>

</body>

</html>
