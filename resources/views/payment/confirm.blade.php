@include('payment.security')

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=viewportdevice-width, initial-scale=1.0">
  <meta name="description" content="A Contdescriptionent Management System designed by horizontech.biz">
  <meta name="author" content="horizoauthorntech.biz">

  <link rel="shortcut icon" href="{{url('')}}/assets/images/favicon3.ico">

  <title>Confirm Payment</title>

  <link href="{{url('')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{url('')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{url('')}}/assets/js/modernizr.min.js"></script>
  <style>
    .bg-dark-theme
    {
      background: #92000e !important;

    }
  </style>
</head>

<body>
{{-- {{dd($data['full_name'])}} --}}
<!-- Start content -->
<div class="content">
    <div class="container">
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        {{-- <span class="ti-user mr-2"></span>Confirm Payment</h3> --}}
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row ">
                                    <form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/pay" method="post">
                                    <input type="hidden" id="access_key" name="access_key" value="{{$data['access_key']}}"/>
                                    <input type="hidden" id="profile_id" name="profile_id" value="{{$data['profile_id']}}0"/>
                                    <input type="hidden" id="transaction_uuid" name="transaction_uuid" value="{{$data['transaction_uuid']}}"/>
                                    <input type="hidden" id="signed_field_names" name="signed_field_names" value="{{$data['signed_field_names']}}"/>
                                    <input type="hidden" id="unsigned_field_names" name="unsigned_field_names" value="{{$data['unsigned_field_names']}}"/>
                                    <input type="hidden" id="signed_date_time" name="signed_date_time" value="{{$data['signed_date_time']}}"/>
                                    <input type="hidden" id="locale" name="locale" value="{{$data['locale']}}"/>
                                    <input type="hidden" id="transaction_type" name="transaction_type" value="{{$data['transaction_type']}}"/>
                                    <input type="hidden" id="reference_number" name="reference_number" value="{{$data['reference_number']}}"/>
                                    <input type="hidden" id="bill_to_forename" name="bill_to_forename" value="{{$data['bill_to_forename']}}"/>
                                    <input type="hidden" id="bill_to_surname" name="bill_to_surname" value="{{$data['bill_to_surname']}}"/>
                                    <input type="hidden" id="bill_to_email" name="bill_to_email" value="{{$data['bill_to_email']}}"/>
                                    <input type="hidden" id="bill_to_phone" name="bill_to_phone" value="{{$data['bill_to_phone']}}"/>
                                    <input type="hidden" id="bill_to_address_line1" name="bill_to_address_line1" value="{{$data['bill_to_address_line1']}}"/>
                                    <input type="hidden" id="bill_to_address_city" name="bill_to_address_city" value="{{$data['bill_to_address_city']}}"/>
                                    <input type="hidden" id="bill_to_address_country" name="bill_to_address_country" value="{{$data['bill_to_address_country']}}"/>
                                    <input type="hidden" id="bill_to_address_postal_code" name="bill_to_address_postal_code" value="{{$data['bill_to_address_postal_code']}}"/>
                                    <input type="hidden" id="bill_to_address_state" name="bill_to_address_state" value="{{$data['bill_to_address_state']}}"/>
                                    <input type="hidden" id="amount" name="amount" value="{{$data['amount']}}"/>
                                    <input type="hidden" id="consumer_id" name="consumer_id" value="{{$data['consumer_id']}}"/>
                                    <input type="hidden" id="merchant_defined_data1" name="merchant_defined_data1" value="{{$data['merchant_defined_data1']}}"/>
                                    <input type="hidden" id="merchant_defined_data2" name="merchant_defined_data2" value="{{$data['merchant_defined_data2']}}"/>
                                    <input type="hidden" id="merchant_defined_data4" name="merchant_defined_data4" value="{{$data['merchant_defined_data4']}}"/>
                                    <input type="hidden" id="merchant_defined_data5" name="merchant_defined_data5" value="{{$data['merchant_defined_data5']}}"/>
                                    <input type="hidden" id="merchant_defined_data7" name="merchant_defined_data7" value="{{$data['merchant_defined_data7']}}"/>
                                    <input type="hidden" id="merchant_defined_data20" name="merchant_defined_data20" value="{{$data['merchant_defined_data20']}}"/>
                                    <input type="hidden" id="customer_ip_address" name="customer_ip_address" value="{{$data['customer_ip_address']}}1"/>
                                    <input type="hidden" id="currency" name="currency" value="{{$data['currency']}}"/>
                                    <input type="hidden" id="submit" name="submit" value="Submit"/>
                                    <input type="hidden" id="returns_accepted" name="returns_accepted" value="{{$data['returns_accepted']}}"/>
                                    <input type="hidden" id="signature" name="signature" value="{{sign($data)}}"/>
                                    <input type="submit" id="submit" value="Confirm"/>

                                 </form>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
<!-- container -->
</div>
<!-- content -->
<!-- jQuery  -->
<script src="{{url('')}}/assets/js/jquery.min.js"></script>
<script src="{{url('')}}/assets/js/popper.min.js"></script>
<!-- Popper for Bootstrap -->
<script src="{{url('')}}/assets/js/bootstrap.min.js"></script>
<script src="{{url('')}}/assets/js/detect.js"></script>
<script src="{{url('')}}/assets/js/fastclick.js"></script>
<script src="{{url('')}}/assets/js/jquery.slimscroll.js"></script>
<script src="{{url('')}}/assets/js/jquery.blockUI.js"></script>
<script src="{{url('')}}/assets/js/waves.js"></script>
<script src="{{url('')}}/assets/js/wow.min.js"></script>
<script src="{{url('')}}/assets/js/jquery.nicescroll.js"></script>
<script src="{{url('')}}/assets/js/jquery.scrollTo.min.js"></script>

<!-- lOGIN Page Plugins -->
<script src="{{url('')}}/assets/pages/login/EasePack.min.js"></script>
<script src="{{url('')}}/assets/pages/login/rAF.js"></script>
<script src="{{url('')}}/assets/pages/login/TweenLite.min.js"></script>
<script src="{{url('')}}/assets/pages/login/login.js"></script>

<script src="{{url('')}}/assets/js/jquery.core.js"></script>
<script src="{{url('')}}/assets/js/jquery.app.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function () {

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

    fetch('https://ipapi.co/json/')
    .then(response => response.json())
    .then((response) => {
      $('#loginForm').append(`<input type="hidden" name="last_login_details" value='`+JSON.slast_login_detailstringify(response)+`'>`);
    })
  });
</script>

</body>

</html>
