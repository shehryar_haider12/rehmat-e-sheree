@extends('layouts.masterpage')

@section('title', 'Compose')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="{{url('')}}/plugins/summernote/summernote-bs4.css">
<style>
  .note-editor.note-frame .note-editing-area .note-editable,
  .note-editor.note-airframe .note-editing-area .note-editable {
    min-height: 200px !important;
  }

  .form-group.ar {
    border-bottom: 1px solid #80808073;
  }
</style>
@endsection

@section('header')
@parent
@endsection

@section('leftsidebar')
@parent
@endsection

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <button class="btn btn-dark-theme waves-effect waves-light"
                        style="background-color: #dc3535 !important" type="button"
                        onclick="window.history.back(1)"><span class="btn-label"><i
                                class="fa fa-arrow-left"></i></span>Go back</button>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{route('categories')}}">Notifcation</a>
                    </li>
                    <li class="breadcrumb-item active">View</li>
                </ol>

            </div>
        </div>

            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">
                        <span class="ti-user mr-2"></span>View Notification</h3>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row">
                                    <div class="col-md-2">
                                      <img src="{{url('').'/uploads/'}}{{$notification->toContact->avatar ?? 'placeholder.jpg'}}"
                                        height="200px">
                                    </div>
                                    <div class="col-md-4">
                                      {{-- <div class="row"> --}}
                                      <div class="form-group ar" style="margin-top: 10px">
                                        <span for="full_name">Full Name: </span>
                                        <span for="full_name">{{$notification->toContact->name}}</span>
                                      </div>
                                      {{-- </div> --}}
                                      <div class="form-group ar" style="margin-top: 34px">
                                        <span for="email">Email: </span>
                                        <span for="email">{{$notification->toContact->email}}</span>
                                      </div>
                                      <div class="form-group ar" style="margin-top: 34px">
                                        <span for="address">Date: </span>
                                        @php
                                        $date = date('D d M Y, H:m a', strtotime($notification->created_at));
                                        @endphp
                                        <span for="address">{{$date}}</span>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="margin-top: 34px">
                                    <span for="message" style="border-bottom:1px solid #80808073;">Message:</span>
                                    <span for="message"> {{$notification->message}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
<!-- container -->
</div>
<!-- content -->
@endsection

@section('rightsidebar')
@parent
@endsection

@section('bottom-mid-scripts')
<script src="{{ url('') }}/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="{{url('')}}/plugins/summernote/summernote-bs4.min.js" type="text/javascript"></script>
@endsection

@section('bottom-bot-scripts')
<script type="text/javascript" src="{{url('')}}/plugins/parsleyjs/parsley.min.js"></script>
<script>
    jQuery(document).ready(function () {

        $('form').parsley();
        $('.select2').select2();
        $('#compose-textarea').summernote()

        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });

    });

</script>
@endsection
