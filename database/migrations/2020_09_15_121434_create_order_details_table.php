<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_no',255);
            $table->string('technosys_order_no',255);
            $table->string('client_order_id',255);
            $table->bigInteger('user_id');
            $table->string('delievery_charges',255)->nullable();
            $table->bigInteger('order_deliver_type_id')->nullable();
            $table->bigInteger('payment_mode_id')->nullable();
            $table->string('remarks',255)->nullable();
            $table->string('total_amount',255);
            $table->string('phone',255);
            $table->double('address_lat');
            $table->double('address_long');
            $table->string('address',255);
            $table->tinyInteger('status')->default(0);
            $table->date('date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
