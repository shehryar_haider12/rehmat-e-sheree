<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiderAssginedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_assgined_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rider_id');
            $table->string('technosys_order_no',255);
            $table->tinyInteger('is_collect')->default(0);
            $table->timestamp('collect_at')->nullable();
            $table->enum('delivery_status', ['in-process', 'on-way','delivered'])->nullable();
            $table->timestamp('delivered_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_assgined_orders');
    }
}
