<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rider_id');
            $table->string('name');
            $table->string('contact',15);
            $table->string('password')->nullable();
            $table->string('device_token')->nullable();
            $table->string('branch', 50)->nullable();
            $table->string('duty_timings', 50)->nullable();
            $table->tinyInteger('status')->default(1);
            // $table->enum('status', ['in-process', 'on-way','delivered']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_details');
    }
}
