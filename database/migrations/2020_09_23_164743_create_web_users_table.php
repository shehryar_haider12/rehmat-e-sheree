<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_information_id')->nullable();
            $table->string('name',255);
            $table->string('phone',11);
            $table->string('password',255);
            $table->string('loyalty_code',255);
            $table->string('device_token',255)->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 is active | 0 is not active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_users');
    }
}
