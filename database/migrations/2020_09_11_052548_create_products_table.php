<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('unit_id')->nullable();
            $table->string('code',255)->nullable();
            $table->string('name',255)->nullable();
            $table->string('featured_image',255)->nullable();
            $table->string('price',255)->nullable();
            $table->string('weight',255)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('is_featured')->default(0);
            $table->tinyInteger('customize_box')->default(0)->comment('0 is not show on box | 1 is show on box');
            $table->tinyInteger('status')->default(1);
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
