-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: 165.227.69.207    Database: rehmat-e-sheree
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_by` tinyint(4) NOT NULL,
  `is_visible` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Desserts','7QEktmmJcXwdJl4GHV0fXJ6Al72VxCzjrb9JvLMx.jpeg',1,0,1,1,1,NULL,'2020-09-21 07:04:29','2020-09-21 07:04:29'),(2,'Mithai','98gH9ATM62q7pQ3Nnpm3sxNwElnCqjek1hsLOApS.jpeg',2,1,1,1,1,NULL,'2020-09-21 07:28:00','2020-10-16 10:29:50'),(3,'Snakes','xIH7hu1PVG2Oke5r2bpauuDk8JKKEnACRC37Kj1E.jpeg',3,1,1,1,1,NULL,'2020-09-22 10:57:17','2020-09-22 10:57:17'),(4,'Cakes','R6gFIEtPe1nzUb8DlkuvdimFNCA6OV4IbbmtCVso.jpeg',3,0,1,1,1,NULL,'2020-10-16 10:31:06','2020-10-16 10:31:06'),(5,'Biscuits or Cookies','MuIF9GnHQZRHJeOTFHltEER8Mk0XMyndtH7wfXai.jpeg',4,0,1,1,1,NULL,'2020-10-16 10:32:25','2020-10-16 10:32:25'),(6,'Pastries','BKJ0J1oi996fK1ZKI70PdLXwEZtXu0jDHepWOY9r.jpeg',6,0,1,1,1,NULL,'2020-10-16 10:33:48','2020-10-16 10:33:48');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_products`
--

DROP TABLE IF EXISTS `category_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_products`
--

LOCK TABLES `category_products` WRITE;
/*!40000 ALTER TABLE `category_products` DISABLE KEYS */;
INSERT INTO `category_products` VALUES (21,3,2,'2020-09-22 10:59:28','2020-09-22 10:59:28'),(27,1,1,'2020-09-22 11:22:21','2020-09-22 11:22:21'),(28,2,1,'2020-09-22 11:22:21','2020-09-22 11:22:21'),(29,4,3,'2020-10-16 10:34:31','2020-10-16 10:34:31'),(30,4,4,'2020-10-16 10:38:18','2020-10-16 10:38:18'),(31,6,4,'2020-10-16 10:38:18','2020-10-16 10:38:18'),(32,1,5,'2020-10-16 10:42:04','2020-10-16 10:42:04'),(33,1,6,'2020-10-16 10:44:58','2020-10-16 10:44:58'),(34,2,6,'2020-10-16 10:44:58','2020-10-16 10:44:58'),(35,5,7,'2020-10-16 10:48:43','2020-10-16 10:48:43'),(36,5,8,'2020-10-16 10:50:31','2020-10-16 10:50:31');
/*!40000 ALTER TABLE `category_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_password_resets`
--

DROP TABLE IF EXISTS `customer_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_password_resets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_password_resets_email_index` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_password_resets`
--

LOCK TABLES `customer_password_resets` WRITE;
/*!40000 ALTER TABLE `customer_password_resets` DISABLE KEYS */;
INSERT INTO `customer_password_resets` VALUES (1,'shehryar.haider0316@gmail.com','4qlNX6e5OEWvWCsTQciGwrObmmJIbVxb','2020-09-30 10:04:14','2020-09-30 09:30:57');
/*!40000 ALTER TABLE `customer_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (78,'2014_10_12_000000_create_users_table',1),(79,'2014_10_12_100000_create_password_resets_table',1),(80,'2019_08_19_000000_create_failed_jobs_table',1),(81,'2020_09_11_052537_create_categories_table',1),(82,'2020_09_11_052548_create_products_table',1),(83,'2020_09_11_053452_create_category_products_table',1),(84,'2020_09_14_061019_create_product_images_table',1),(85,'2020_09_15_092718_create_orders_table',1),(86,'2020_09_15_121434_create_order_details_table',1),(87,'2020_09_21_111840_create_units_table',1),(88,'2020_09_22_164548_create_settings_table',2),(89,'2020_09_23_164743_create_web_users_table',3),(90,'2016_06_01_000001_create_oauth_auth_codes_table',4),(91,'2016_06_01_000002_create_oauth_access_tokens_table',4),(92,'2016_06_01_000003_create_oauth_refresh_tokens_table',4),(93,'2016_06_01_000004_create_oauth_clients_table',4),(94,'2016_06_01_000005_create_oauth_personal_access_clients_table',4),(95,'2020_09_30_132125_create_customer_password_resets_table',5),(96,'2020_10_14_105730_create_rider_details_table',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0903e3b5c22f948c8b24db7d7e9ecc00f1b6e5d143f96c5921d5b50f082455bb111ecf4cbc2baab8',6,1,'MyApp','[]',0,'2020-09-24 09:28:48','2020-09-24 09:28:48','2021-09-24 14:28:48'),('11bc85bb49629afbb7722a61876622e15c7ac72000716c57617a87b0458632d2f178163a25c186f7',4,1,'MyApp','[]',0,'2020-09-25 07:41:01','2020-09-25 07:41:01','2021-09-25 12:42:01'),('1e3d5dfffdd9b95b0bb7c601799fb1126d877898f06adca4b1ee2df2d35df7bdd931fa71e94b6792',7,1,'MyApp','[]',0,'2020-10-16 07:56:32','2020-10-16 07:56:32','2021-10-16 12:56:32'),('239d51dc3be850b76c61d4a80243ae8e293d93d8fd0f54cf9dd933333cff8d9b806bd1f6c44fb144',7,1,'MyApp','[]',0,'2020-10-16 07:45:53','2020-10-16 07:45:53','2021-10-16 12:45:53'),('36015256d18f642c71ef22c8de95392244e5ee3d449c8442f3aa10a57918980871cc5230deac4c59',8,1,'MyApp','[]',0,'2020-10-16 11:30:05','2020-10-16 11:30:05','2021-10-16 16:30:05'),('37d0c3841e971982d795ebde69ffcd89775471dc8a883b1b950b4d20bb23bb4a689163bf1d36d256',8,1,'MyApp','[]',0,'2020-10-16 11:02:36','2020-10-16 11:02:36','2021-10-16 16:02:36'),('41103b00c85acd85a3ea7434541139b7fffc0e31c59c01e772237947fa2da29be4bb7610a58ec971',5,1,'MyApp','[]',0,'2020-09-25 07:37:39','2020-09-25 07:37:39','2021-09-25 12:38:39'),('4b102cd375da88e926a92b23cbfa177d340ba12e6e6e5d8eadeb178e74d21d69252f3078d3219b67',8,1,'MyApp','[]',0,'2020-10-16 10:35:50','2020-10-16 10:35:50','2021-10-16 15:35:50'),('578e18c4c41927f27376f2c700e23a5a0962ef3a41718ac82ff2e6be59e55fda05738354e2cae5cb',8,1,'MyApp','[]',0,'2020-10-16 08:10:29','2020-10-16 08:10:29','2021-10-16 13:10:29'),('5f659e7966c65e5e3e6afd7117c72e2f160f5c1297fcd2dda502d6ba0ec785ddc152a48e9307ab41',6,1,'MyApp','[]',0,'2020-09-24 09:30:56','2020-09-24 09:30:56','2021-09-24 14:30:56'),('5fbe68cbc370d0d755d901389e1cf2ef0f82fd44f8bcefd48c18a335340a8753920c9402635dc32c',5,1,'MyApp','[]',0,'2020-09-25 07:34:04','2020-09-25 07:34:04','2021-09-25 12:34:04'),('61e1276013f5278450aac6c6aa8f11a1238f03714e3e5f18eb6d1674710d6f0f2e9fdf4f0d1a5e2c',8,1,'MyApp','[]',0,'2020-10-16 08:02:17','2020-10-16 08:02:17','2021-10-16 13:02:17'),('650929f805b61f0c94a02ea65b21573d6d1e46861d5b179a3a3564939785307f90bc713c7773f8ec',7,1,'MyApp','[]',0,'2020-10-16 07:45:42','2020-10-16 07:45:42','2021-10-16 12:45:42'),('6834e3668ff52b5318862c49e815215f2e64fb0fe277a102c9098c13990842376454150eb5c8c953',8,1,'MyApp','[]',0,'2020-10-16 11:39:30','2020-10-16 11:39:30','2021-10-16 16:39:30'),('6d65d4d49fe408b1b92a4a8eadf019ded8feaf69df4357a5b5094c971de9cd8813016b4b20f30701',8,1,'MyApp','[]',0,'2020-10-16 10:25:16','2020-10-16 10:25:16','2021-10-16 15:25:16'),('6e87f7a019253fd99a28439b8aa1bd27eec6e1b15daac00dad3d114adbea208091e9bea30dad464b',8,1,'MyApp','[]',0,'2020-10-16 11:06:17','2020-10-16 11:06:17','2021-10-16 16:06:17'),('81113fce84271f8e3e8df72410d4d63ea6067e10bbcb9c5382fe48484295be76fbd437c5a6436132',8,1,'MyApp','[]',0,'2020-10-16 10:09:20','2020-10-16 10:09:20','2021-10-16 15:09:20'),('8b718b2f378e21ec887c81d3d80416a8e2abb4aebc082d2177469ab8449747c9b98d206d550c4e3c',8,1,'MyApp','[]',0,'2020-10-16 09:31:55','2020-10-16 09:31:55','2021-10-16 14:31:55'),('8fddd9e89bd8abdd9a25ac17b8eb2ba3103d2e57b39872770a430199f1c5292ddece86e5e69224a7',8,1,'MyApp','[]',0,'2020-10-16 07:59:42','2020-10-16 07:59:42','2021-10-16 12:59:42'),('916aae8a5291cf1e67767ebc5fc5d6b1fb5d6f25b890d22c60d9af1aabe7f208c2a29f97f8877bf3',3,1,'MyApp','[]',0,'2020-09-24 06:14:55','2020-09-24 06:14:55','2021-09-24 11:14:55'),('95cd464e6df025500527bde9ba0367c606868e536a1b7ff4cc81a80601eec9d0cf3a814e03a419bb',6,1,'MyApp','[]',0,'2020-10-01 06:51:19','2020-10-01 06:51:19','2021-10-01 11:51:19'),('9e9dc58066f49c3b40c70460236e7cf7f6b54036720d2d1346e658704761ae2acee84a979fa86ccf',8,1,'MyApp','[]',0,'2020-10-16 10:51:33','2020-10-16 10:51:33','2021-10-16 15:51:33'),('aa22e46205c2d0ec9c49b52196add66184e5dd46c6249102ee6c817ef56c3d03914bd48faa80e495',7,1,'MyApp','[]',0,'2020-10-16 07:55:49','2020-10-16 07:55:49','2021-10-16 12:55:49'),('b14c106fed46b162229f75deb1019755f810e0a7c2c07567f36e7e5c687a9269681a6ea49ad1d21e',4,1,'MyApp','[]',0,'2020-09-25 07:47:33','2020-09-25 07:47:33','2020-09-25 12:48:33'),('b2fa909430f3be03aba40d61e82c732ed49cc9b21aee4acce83a82cbef1a2634dd434aa12a6c8e59',4,1,'MyApp','[]',0,'2020-09-25 10:24:46','2020-09-25 10:24:46','2020-09-25 15:25:46'),('bd8e71c5dfa844972b1c05d4f3904bb1d9725bacd8ad1e0429024417ef591a37d7bfdb9993f3ae75',7,1,'MyApp','[]',0,'2020-10-16 07:56:14','2020-10-16 07:56:14','2021-10-16 12:56:14'),('c34b406e2f6bb62c40af159f9a9022fa663998f758ca7e8c47a0a29d7450cb104554aa6533e07681',4,1,'MyApp','[]',0,'2020-09-25 07:55:10','2020-09-25 07:55:10','2020-09-25 12:56:10'),('d650f18a5fa9318ad07f2ad5988a18d9e507e46ac4aecfb7bab9883bd08d339a7831ef57102df631',8,1,'MyApp','[]',0,'2020-10-16 11:28:20','2020-10-16 11:28:20','2021-10-16 16:28:20'),('e07b92721fb40b8123a2e22f16b62aaeac706945253f072e2cd28f126b7628ab547ea49c21b0fe66',8,1,'MyApp','[]',0,'2020-10-16 09:25:00','2020-10-16 09:25:00','2021-10-16 14:25:00'),('e74bddb6b8aabb4b935c9d9a5004f460bf95750fdf16e457ea587d45817dd9ebd2a37a01d081827e',6,1,'MyApp','[]',0,'2020-10-01 06:50:32','2020-10-01 06:50:32','2021-10-01 11:50:32'),('e817b0a7e4c9ad9a01634262c4f4736d1acf072e848d8540daa2a6065f3a03ac4ce209f72bbd1ca2',8,1,'MyApp','[]',0,'2020-10-16 10:08:21','2020-10-16 10:08:21','2021-10-16 15:08:21'),('e9629bb7ac51432d57919e33a139106664f74978d17857c07e571430e2a6c360d2d79824948bb33d',8,1,'MyApp','[]',0,'2020-10-16 07:59:47','2020-10-16 07:59:47','2021-10-16 12:59:47'),('edf3b15eace0e048d1c35837a13bd30fc831aff995bd6e41bab72889d77286abf10125af1688fb61',5,1,'MyApp','[]',0,'2020-09-24 08:18:23','2020-09-24 08:18:23','2021-09-24 13:18:23'),('f6f9e62641f6bed010b65c8887dfc6f6219c5ef6ac56a672173e2be40054b8349843518d6876c7e9',8,1,'MyApp','[]',0,'2020-10-16 09:28:28','2020-10-16 09:28:28','2021-10-16 14:28:28'),('f9431588c501daf7a9ce5a0f8e0e5c4193987ca7f971c5e36e0ed1d1ef51764c43271f138b10ec73',7,1,'MyApp','[]',0,'2020-10-16 07:56:54','2020-10-16 07:56:54','2021-10-16 12:56:54'),('fbe5d5c00d02f9b4a0f8fdebbafc78119618e07f5fadcfe29dee5d8dca2b02aa240b457442f7a440',8,1,'MyApp','[]',0,'2020-10-16 08:06:03','2020-10-16 08:06:03','2021-10-16 13:06:03');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','LLs308coBy5OPPR1JIM0N64alZdfnOEwAp3bG4bs',NULL,'http://localhost',1,0,0,'2020-09-24 05:24:10','2020-09-24 05:24:10'),(2,NULL,'Laravel Password Grant Client','RiHjYDrKdSWDWUYK3MnhXcbYfuIMbn2KSkca3M6g','users','http://localhost',0,1,0,'2020-09-24 05:24:10','2020-09-24 05:24:10');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-09-24 05:24:10','2020-09-24 05:24:10');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `technosys_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `delievery_charges` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `total_amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (3,'20200922C9A3','DFW12',4,'0','120000','031231231','karachi pakistan',0,'2020-09-22',NULL,'2020-09-22 03:17:42','2020-09-22 03:17:42'),(11,'202009239E7E','123122',5,'60','1040','03120232123','house 34/9, area 3-b landhi 4 karachi',0,'2020-09-23',NULL,'2020-09-23 06:49:53','2020-09-23 06:49:53');
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (12,1,'20200922C9A3','1','1200',NULL,'2020-09-22 03:17:42','2020-09-22 03:17:42'),(13,1,'20200922C9A3','2','1200',NULL,'2020-09-22 01:17:49','2020-09-22 01:17:49'),(45,2,'202009239E7E','12','180',NULL,'2020-09-23 06:49:53','2020-09-23 06:49:53'),(46,3,'202009239E7E','1','800',NULL,'2020-09-23 06:49:53','2020-09-23 06:49:53');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('shehryar@hashlob.com','$2y$10$6vNnANtRCsoPYvmM9Ilu1eVHXtX.bPmJ1Om6AJ6N193BB9.AUhvC2',NULL,'2020-09-30 08:13:07');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (1,1,'J7aa5VCvGA8PuDkg8rHqBmskk83UaabyT678neAM.jpeg','2020-09-21 07:30:41','2020-09-21 07:27:30','2020-09-21 07:30:41'),(2,1,'lttsSruXv5xsgnxvuPkKsQcCrkWqYgqZ2EcdL0H2.jpeg','2020-09-21 07:30:41','2020-09-21 07:27:30','2020-09-21 07:30:41'),(3,1,'BFaF1GruJLwfTOKlLb9IIbEzejFX92UkJ5dmBrSM.jpeg','2020-09-21 07:30:41','2020-09-21 07:27:30','2020-09-21 07:30:41'),(4,1,'fO6kW70dgdHz1PntzvMGAmBa3yDrxxLjJZ31wBfq.jpeg','2020-09-22 11:22:06','2020-09-22 01:19:47','2020-09-22 11:22:06'),(5,1,'quKy3y0tjHm4jAaiRs7gTtiCandZKM6zZCDUEVut.jpeg','2020-09-22 11:22:09','2020-09-22 01:19:47','2020-09-22 11:22:09'),(6,2,'e3da3Q9trzlFupHf6LeLkOuxH2hiqzMJohq7WqTf.jpeg',NULL,'2020-09-22 10:59:03','2020-09-22 10:59:03'),(7,2,'qxiKCRJktw8f2y0pwHbSs5FlBp3p9GqsULDHuKxJ.jpeg',NULL,'2020-09-22 10:59:03','2020-09-22 10:59:03'),(8,3,'P8x1GpQ0sbooXpD91ZKjUHZfrdQcrTxa7DRoBH9X.jpeg',NULL,'2020-09-22 11:01:56','2020-09-22 11:01:56'),(9,3,'p0TWFQeXdZHpIWFlnLF4PkJ9zh5UEQW3GSdaOFFW.jpeg',NULL,'2020-09-22 11:01:56','2020-09-22 11:01:56'),(10,4,'XvJ64jHfujcb2rEr4TGBaNjsVsP16nkVy2Hc73mq.jpeg',NULL,'2020-10-16 10:38:18','2020-10-16 10:38:18'),(11,5,'dXBs4Qwro42HknbmS1roxmhuTpH286o3p9bTBLZK.jpeg',NULL,'2020-10-16 10:42:04','2020-10-16 10:42:04'),(12,6,'c16qWgg2ElT9lEe9okCoRzYBljwLEUWhW3QZDfd9.jpeg',NULL,'2020-10-16 10:44:58','2020-10-16 10:44:58'),(13,7,'GP0tKj49whmJyYYmuTA7jvIj0zachiDamRfE03Rw.jpeg',NULL,'2020-10-16 10:48:43','2020-10-16 10:48:43'),(14,8,'klANCcFnjfcGzSHoWd2B5sTU8prKJugIlhSDvuHY.jpeg',NULL,'2020-10-16 10:50:31','2020-10-16 10:50:31');
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unit_id` bigint(20) NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,3,'JBHX23122','Gulaab Jamun','qs23il9HrZupsiVxc2dd2qvTLxMZRGonJbjDolko.jpeg','40','35','asf asdf fds f df fad fd fsad fds asdf dsaddfdaf sdf',1,1,1,NULL,'2020-09-21 07:27:30','2020-09-22 11:22:21'),(2,4,'SMA112','Samoosa','OvoSOlYzF4tuuBK64x3igw7sD2kQSA2lubyhtFnE.jpeg','15','1','samoosa sadfa sdfasdf asfsa fasdfa dfdf dfdf asdf sadf',1,1,1,NULL,'2020-09-22 10:59:03','2020-09-22 10:59:28'),(3,2,'CA22K','Vanilla Cake','6RWJ42Zdl2u09MvmVTVOa653RvH6vaewu8DoUDEk.jpeg','800','1','dd fasdf asdfas dfasdfas dfasd fasdf s dfsadfsd',1,1,1,NULL,'2020-09-22 11:01:56','2020-10-16 10:34:31'),(4,4,'CH22P1','Chocolate Pastries','TUhOMtYDpWHXSocCOUKaGlvasefvDLm1nQTbNMRM.jpeg','30','1','dfa fd fasdf sdfd fsdaf adsfs fsd ds ads f dsafsadfs adfd fd af d adsa dsf dfd f',1,1,1,NULL,'2020-10-16 10:38:18','2020-10-16 10:38:18'),(5,4,'DE2CA11','Candies','NUq2FJSf57im4hcjQdzwVx6vuEA0VFbYSR2Lczfd.jpeg','80','1','dfadskfdjs fdsfsad fdssd df dffasdfsdfdsfasd f dsfasdfdsf dsfasdf sdfasdfsd',1,1,1,NULL,'2020-10-16 10:42:04','2020-10-16 10:42:04'),(6,1,'DE1MI2K2','Kalakand','A0oaTUMREn2PfWjwRV5NOrXGoNTCPtITHTNIqWns.jpeg','580','1','dfasdf d saf sdf d ad fsad f sdf da d f asd fads fds fsd afds  sd dsf d',1,1,1,NULL,'2020-10-16 10:44:58','2020-10-16 10:44:58'),(7,3,'BI2CO1CC','Coconut Biscuits','bLEXRXZqOYfwvbjyNxr58N7wH7YGbGvF8Q8ihttJ.jpeg','60','3','sdfad ff ddsf asdf df sdafsdfd fasdfa sdfasfd',1,1,1,NULL,'2020-10-16 10:48:43','2020-10-16 10:48:43'),(8,3,'BI22C02EG1','Egg Biscuit','PthJ6B6bP21j5pbImbjKSqo8FuQBgYOA32QsDL3k.jpeg','120','1','dfa fdasd fdf dsadf sdfasd ds f asdf dsf adsf asdf dsf asdf df',1,1,1,NULL,'2020-10-16 10:50:31','2020-10-16 10:50:31');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rider_details`
--

DROP TABLE IF EXISTS `rider_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rider_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rider_id` bigint(20) NOT NULL,
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rider_details`
--

LOCK TABLES `rider_details` WRITE;
/*!40000 ALTER TABLE `rider_details` DISABLE KEYS */;
INSERT INTO `rider_details` VALUES (1,12,'35351321351','Shehryar','03120252121',NULL,'2020-10-14 06:44:50','2020-10-14 06:44:50'),(2,12,'465457521','Shehryar Ahmed','03120252123',NULL,'2020-10-14 06:47:51','2020-10-14 06:47:51');
/*!40000 ALTER TABLE `rider_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'delievery_charges','60',1,1,'2020-09-22 12:25:01','2020-09-22 12:29:59'),(3,'free_delievery_charges_after','1200',1,1,'2020-09-22 12:29:46','2020-09-23 06:43:49');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,'KG',1,1,1,NULL,'2020-09-21 07:22:26','2020-09-21 07:23:11'),(2,'Pounds',1,1,2,NULL,'2020-09-21 07:33:25','2020-09-21 07:34:15'),(3,'GARAM',1,1,1,NULL,'2020-09-22 10:53:47','2020-09-22 10:53:47'),(4,'Pieces',1,1,1,NULL,'2020-09-22 10:59:17','2020-09-23 06:53:52');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Shehryar Haider','5sqLdG1EqwTtXFYqPm4mFUWUWVwnwzUV48XDgkfR.jpeg','shehryar@hashlob.com',NULL,'$2y$10$Y2ggsnvSy3z2ucEvuva9XutUafZIalZxJVdVGuRgT1EGwAhYUng0q',NULL,NULL,'2020-09-21 07:04:01','2020-09-21 07:04:09');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_users`
--

DROP TABLE IF EXISTS `web_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `loyalty_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_users`
--

LOCK TABLES `web_users` WRITE;
/*!40000 ALTER TABLE `web_users` DISABLE KEYS */;
INSERT INTO `web_users` VALUES (4,'Muhammad Arsalan','arsalan@email.com','12345678','2132khj',NULL,'2020-09-24 06:24:30','2020-09-24 06:24:30'),(5,'Shehryar','shehryar.haider0316@gmail.com','$2y$10$IYlda7o5OT.W1opsvLo2ue84DAiNc1KrmW07bVP51R8x3Pvj3gCLG','hello',NULL,'2020-09-24 08:18:23','2020-09-30 10:04:14'),(6,'Muhammad Ahsun','m.ahsun@email.com','12345678','hello',NULL,'2020-09-24 09:28:48','2020-09-24 09:28:48'),(7,'joe','joe@gmail.com','12345678','8152',NULL,'2020-10-16 07:45:42','2020-10-16 07:45:42'),(8,'jeo','jeo@gmail.com','12345678','8152',NULL,'2020-10-16 07:59:42','2020-10-16 07:59:42');
/*!40000 ALTER TABLE `web_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-16 17:04:31
