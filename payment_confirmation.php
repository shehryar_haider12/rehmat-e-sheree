<?php
// print_r($_REQUEST);
if(!isset($_REQUEST['bill_to_forename']))
{
    exit;
}
?>
<?php include 'security.php' ?>

<html>
<head>
    <title>Secure Acceptance - Payment Confirmation</title>
    <link href="<?php echo 'public/'?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/pay" method="post"/>

<?php
    foreach($_REQUEST as $name => $value) {
        $params[$name] = $value;
    }
?>
<fieldset id="confirmation">
    <div class="container">
    <hr>
        <button class="btn btn-success col-md-10">Proceed</button>
    <hr>
        <?php
            // foreach($params as $name => $value) {
            //         echo "<div>";
            //         echo "<span class=\"fieldName\">" . $name . "</span><span class=\"fieldValue\">" . $value . "</span>";
            //         echo "</div>\n";
            //     }
                ?>
    </div>
</fieldset>
    <?php
        foreach($params as $name => $value) {
            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
        }
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
        ?>
        <!-- <input type="submit"  value="submit" id="submit"> -->
</form>
</body>
</html>
 