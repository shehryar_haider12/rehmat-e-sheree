<?php
// print_r($_REQUEST);
if(!isset($_REQUEST['bill_to_forename']))
{
    exit;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A Content Management System designed by horizontech.biz">
    <meta name="author" content="horizontech.biz">

    <link rel="shortcut icon" href="<?php echo 'public/'?>/assets/images/favicon3.ico">

    <title>Confirm Payment</title>

    <link href="<?php echo 'public/'?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo 'public/'?>/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo 'public/'?>/assets/css/style.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo 'public/'?>/assets/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo 'public/'?>/assets/css/custom.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo 'public/'?>/assets/js/modernizr.min.js"></script>
    <style>
        .bg-dark-theme {
            background: #92000e !important;

        }

    </style>
</head>

<body>

    <?php //date_default_timezone_set("America/Los_Angeles");  echo date("Y-m-d H:i:s");
?>
    <div class="content">
        <div class="container">
            <div class="portlet">
                <div class="portlet-heading bg-light-theme">
                    <h3 class="portlet-title">

                        <div class="portlet-widgets">
                            <span class="divider"></span>
                        </div>
                        <div class="clearfix"></div>
                </div>

                <div id="bg-inverse" class="panel-collapse collapse show" style="">
                    <div class="portlet-body">

                        <div class="card-box">
                            <div class="section">
                                <div class="row ">
                                    <form role="form" id="payment_form" class="col-md-12"
                                        action="payment_confirmation.php" method="post">
                                        <div class="head">
                                            <h2 class="text-center">Confirm Payment</h2>
                                            <hr>
                                        </div>
                                        <input type="hidden" name="access_key" value="87b65912a93a338888aa9072c3509d7e">
                                        <input type="hidden" name="profile_id"
                                            value="9399E91E-7D2C-44FA-83F0-C325D4800C60">
                                        <input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">


                                        <input type="hidden" name="signed_field_names"
                                            value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,bill_to_address_country,bill_to_address_state,bill_to_address_postal_code,bill_to_address_city,bill_to_address_line1,bill_to_forename,bill_to_surname,bill_to_email,consumer_id,merchant_defined_data1,merchant_defined_data2,merchant_defined_data4,merchant_defined_data5,merchant_defined_data7,merchant_defined_data20,customer_ip_address,bill_to_phone,amount,currency,ship_to_address_city,ship_to_address_country,ship_to_address_line1,ship_to_address_postal_code,ship_to_address_state,ship_to_forename,ship_to_phone,ship_to_surname">
                                        <input type="hidden" name="unsigned_field_names">
                                        <input type="hidden" name="signed_date_time"
                                            value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                                        <input type="hidden" name="locale" value="en">
                                        <input type="hidden" name="transaction_type" size="25" value="sale">
                                        <input type="hidden" name="reference_number" size="25"
                                            value="<?php echo uniqid() ?>">
                                        <div class="row">
                                            <?php
                                                if(isset($_REQUEST['bill_to_forename']))
                                                {
                                                    $name = explode(' ',$_REQUEST['bill_to_forename']);
                                                }
                                            ?>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">First Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_forename"
                                                        id="fname" placeholder="First Name" required="" disabled="true" value="<?php echo $name[0]?>">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Last Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_surname"
                                                        id="lname" placeholder="Last Name" required="" disabled="true" value="<?php echo $name[1] ?? 'not entered'?>">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputEmail">Enter Email</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control" id="InputEmailFirst"
                                                        name="bill_to_email" placeholder="Enter Email" required="" disabled="true" value="<?php echo $_REQUEST['bill_to_email']?>">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputName">Phone</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_phone"
                                                        id="phone" placeholder="Phone" required="" disabled="true" value="<?php echo $_REQUEST['bill_to_phone']?>">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="InputName">Address</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_address_line1"
                                                        id="address" placeholder="Address" required=""  disabled="true" value="<?php echo $_REQUEST['bill_to_address_line1']?>">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="InputName">City</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="bill_to_address_city"
                                                        id="city" placeholder="City" required=""  disabled="true" value="<?php echo $_REQUEST['bill_to_address_city']?>">
                                                    <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>


                                            <div class="form-group col-md-3">
                                                <label for="InputName">Country</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="bill_to_address_country"
                                                        id="country" placeholder="Country"  disabled="true" onchange="checkStateZip();"
                                                        required="">
                                                        <option value="PK" selected>Pakistan</option>
                                                    </select>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div id="statZipDiv" style=""> -->
                                            <div class="form-group col-md-3">
                                                <div>
                                                    <label for="InputName">Postal Code</label>
                                                </div>
                                                <div>
                                                    <input type="text" class="form-control"
                                                        name="bill_to_address_postal_code" id="zip"
                                                        placeholder="Postal code" disabled="true" value="<?php echo $_REQUEST['bill_to_address_postal_code']?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <div>
                                                    <label for="InputName">State</label>
                                                </div>
                                                <div>
                                                    <input type="text" class="form-control" name="bill_to_address_state"
                                                        placeholder="State" value="Sindh" disabled="true">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Amount</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="amount" id="amount"
                                                        placeholder="Amount" required=""  disabled="true" value="<?php echo $_REQUEST['amount']?>">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="InputName">Currency</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="currency"
                                                        placeholder="currency" value="PKR" disabled="true">
                                                    <span class="input-group-addon"><span
                                                            class="glyphicon glyphicon-asterisk"></span></span>
                                                </div>
                                            </div>
                                            <input type="hidden" id="amount" name="amount" value="<?php echo $_REQUEST['amount']?>" />
                                            <input type="hidden" id="bill_to_forename" name="bill_to_forename" value="<?php echo $name[0]?>" />
                                            <input type="hidden" id="bill_to_surname" name="bill_to_surname" value="<?php echo $name[1] ?? 'not entered'?>" />
                                            <input type="hidden" id="bill_to_address_line1" name="bill_to_address_line1" value="<?php echo $_REQUEST['bill_to_address_line1'] ?>" />
                                            <input type="hidden" id="bill_to_address_city" name="bill_to_address_city" value="<?php echo $_REQUEST['bill_to_address_city'] ?>" />
                                            <input type="hidden" id="bill_to_address_country"  name="bill_to_address_country" value="PK" />
                                            <input type="hidden" id="bill_to_address_postal_code" name="bill_to_address_postal_code" value="<?php echo $_REQUEST['bill_to_address_postal_code'] ?>" />

                                            <input type="hidden" id="ship_to_forename" name="ship_to_forename" value="<?php echo $name[0]?>" />
                                            <input type="hidden" id=" ship_to_phone" name=" ship_to_phone" value="<?php echo $_REQUEST['bill_to_phone'] ?>" />
                                            <input type="hidden" id="ship_to_surname" name="ship_to_surname" value="<?php echo $name[1] ?? 'not entered'?>" />
                                            <input type="hidden" id="ship_to_address_line1" name="ship_to_address_line1" value="<?php echo $_REQUEST['bill_to_address_line1'] ?>" />
                                            <input type="hidden" id="ship_to_address_city" name="ship_to_address_city" value="<?php echo $_REQUEST['bill_to_address_city'] ?>" />
                                            <input type="hidden" id="ship_to_address_country"  name="ship_to_address_country" value="PK" />
                                            <input type="hidden" id="ship_to_address_postal_code" name="ship_to_address_postal_code" value="<?php echo $_REQUEST['bill_to_address_postal_code'] ?>" />
                                            <input type="hidden" id="ship_to_address_state" name="ship_to_address_state" value="SN" />
                                            
                                            <input type="hidden" id="bill_to_address_state" name="bill_to_address_state" value="SN" />
                                            <input type="hidden" id="bill_to_email" name="bill_to_email" value="<?php echo $_REQUEST['bill_to_email'] ?>" />
                                            <input type="hidden" id="bill_to_phone" name="bill_to_phone" value="<?php echo $_REQUEST['bill_to_phone'] ?>" />

                                            <input type="hidden" id="amount" name="amount" value="<?php echo $_REQUEST['amount'] ?>" />
                                            <input type="hidden" id="currency" name="currency" value="pkr" />
                                            <!-- </div> -->

                                            <input type="hidden" id="consumer_id" name="consumer_id" value="<?php echo uniqid(); ?>">

                                            <input type="hidden" id="channel of operations"
                                                name="merchant_defined_data1" value="WC">
                                            <input type="hidden" name="merchant_defined_data2" value="YES">
                                            <input type="hidden" name="merchant_defined_data3" value="Sweets">
                                            <input type="hidden" name="merchant_defined_data6" value="Standard">
                                            <input type="hidden" name="merchant_defined_data8" value="Pakistan">
                                            <input type="hidden" name="merchant_defined_data4" value="Sweets">
                                            <input type="hidden" name="merchant_defined_data5" value="Standard">
                                            <input type="hidden" name="merchant_defined_data7" value="NO">
                                            <input type="hidden" name="merchant_defined_data20" value="NO">
                                            <input type="hidden" id="customer_ip_address" name="customer_ip_address"
                                                size="25" value="127.0.0.1">

                                        </div>
                                        <!-- <input type="submit" name="submit" id="submit" value="Submit"
class="btn btn-info pull-right" onclick="donate();"><br><br><br> -->

                                        <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right" onclick="donate();"><br><br><br>
                                        <input type="text" id="decision" name="returns_accepted" value=""
                                            style="visibility:hidden;">
                                </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- content -->
    <!-- jQuery  -->
    <script src="<?php echo 'public/'?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/popper.min.js"></script>
    <!-- Popper for Bootstrap -->
    <script src="<?php echo 'public/'?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/detect.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/fastclick.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/waves.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/wow.min.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/jquery.scrollTo.min.js"></script>

    <!-- lOGIN Page Plugins -->
    <script src="<?php echo 'public/'?>/assets/pages/login/EasePack.min.js"></script>
    <script src="<?php echo 'public/'?>/assets/pages/login/rAF.js"></script>
    <script src="<?php echo 'public/'?>/assets/pages/login/TweenLite.min.js"></script>
    <script src="<?php echo 'public/'?>/assets/pages/login/login.js"></script>

    <script src="<?php echo 'public/'?>/assets/js/jquery.core.js"></script>
    <script src="<?php echo 'public/'?>/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {

            // Init CanvasBG and pass target starting location
            CanvasBG.init({
                Loc: {
                    x: window.innerWidth / 2,
                    y: window.innerHeight / 3.3
                },
            });

            fetch('https://ipapi.co/json/')
                .then(response => response.json())
                .then((response) => {
                    $('#loginForm').append(`<input type="hidden" name="last_login_details" value='` +
                        JSON
                        .stringify(response) + `'>`);
                })
        });

    </script>

</body>

</html>
