importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');
// importScripts('js/firebase.js');


const firebaseConfig = {
  apiKey: "AIzaSyBoMvSlwsx3BEu3wJwi8BoT8dhIT8__V8A",
  authDomain: "push-notifications-77e12.firebaseapp.com",
  databaseURL: "https://push-notifications-77e12.firebaseio.com",
  projectId: "push-notifications-77e12",
  storageBucket: "push-notifications-77e12.appspot.com",
  messagingSenderId: "671803690184",
  appId: "1:671803690184:web:8563c50c5cff2f565969fd",
  measurementId: "G-0CYE62CNCH"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();


// [START on_background_message]
messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});